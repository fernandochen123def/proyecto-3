using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraManager : MonoBehaviour
{
	private static PlayerCameraManager singleton;
	[SerializeField] private Camera playerCamera;

	public static PlayerCameraManager Singleton { get => singleton;}
	public static Camera PlayerCamera { get => singleton.playerCamera;}

	private void Awake()
	{
		if (singleton == null)
		{
			singleton = this;
		}
		else
		{
			Debug.LogFormat("<color=orange>Duplicated component deleted</color>");
			Destroy(this);
		}
	}

	private void Start()
	{
		playerCamera = GetComponent<Camera>();
	}
}
