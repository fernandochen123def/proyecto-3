using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnemySpawner : MonoBehaviour
{
	EnemySpawner enemySpawner;
	private void Awake()
	{
		enemySpawner = FindObjectOfType<EnemySpawner>();
	}
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player")) { enemySpawner.running = true; }
	}
}
