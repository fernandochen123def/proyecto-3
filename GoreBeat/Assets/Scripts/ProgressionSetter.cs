using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProgressionSetter : MonoBehaviour
{
	[Header("Level Buttons")]
	[SerializeField] private Button level1Button;
	[SerializeField] private Button level2Button;
	[SerializeField] private Button level3Button;

	[Header("Fist Buttons")]
	[SerializeField] private Button fist2Button;
	[SerializeField] private Button fist3Button;

	[Header("Scoreboard Card")]
	[SerializeField] private TextMeshProUGUI scoreText;
	[SerializeField] private TextMeshProUGUI timeSurvivedText;
	[SerializeField] private TextMeshProUGUI highestStreakText;
	[SerializeField] private TextMeshProUGUI gradeText;
	//[SerializeField] private GameObject scoreboardCard;

	private void Start()
	{
		UpdateButtonInteractability();
		AddButtonListeners();
	}

	public void UpdateButtonInteractability()
	{
		// Load player progression
		SaveManager.Singleton.Load();

		// Update level buttons
		level1Button.interactable = SaveManager.Singleton.IsFirstLevelUnlocked();
		level2Button.interactable = SaveManager.Singleton.IsSecondLevelUnlocked();
		level3Button.interactable = SaveManager.Singleton.IsThirdLevelUnlocked();

		// Update fist buttons
		fist2Button.interactable = SaveManager.Singleton.IsSecondFistUnlocked();
		fist3Button.interactable = SaveManager.Singleton.IsThirdFistUnlocked();
	}

	private void AddButtonListeners()
	{
		level1Button.onClick.AddListener(() => ShowScoreboard(1));
		level2Button.onClick.AddListener(() => ShowScoreboard(2));
		level3Button.onClick.AddListener(() => ShowScoreboard(3));

		level1Button.GetComponent<HoverEventTrigger>().onHoverEnter.AddListener(() => UpdateScoreboard(1));
		level2Button.GetComponent<HoverEventTrigger>().onHoverEnter.AddListener(() => UpdateScoreboard(2));
		level3Button.GetComponent<HoverEventTrigger>().onHoverEnter.AddListener(() => UpdateScoreboard(3));

		level1Button.GetComponent<HoverEventTrigger>().onHoverExit.AddListener(HideScoreboard);
		level2Button.GetComponent<HoverEventTrigger>().onHoverExit.AddListener(HideScoreboard);
		level3Button.GetComponent<HoverEventTrigger>().onHoverExit.AddListener(HideScoreboard);
	}

	private void ShowScoreboard(int level)
	{
		UpdateScoreboard(level);
		//scoreboardCard.SetActive(true);
	}

	private void UpdateScoreboard(int level)
	{
		SaveManager.Singleton.Load();
		switch (level)
		{
			case 1:
				scoreText.text = $"{SaveManager.Singleton.GetHighScore(1)}";
				timeSurvivedText.text = $"{SaveManager.Singleton.GetTimeSurvived(1)}";
				highestStreakText.text = $"{SaveManager.Singleton.GetHighestStreak(1)}";
				gradeText.text = $"{SaveManager.Singleton.GetGrade(1)}";
				break;
			case 2:
				scoreText.text = $"{SaveManager.Singleton.GetHighScore(2)}";
				timeSurvivedText.text = $"{SaveManager.Singleton.GetTimeSurvived(2)}";
				highestStreakText.text = $"{SaveManager.Singleton.GetHighestStreak(2)}";
				gradeText.text = $"{SaveManager.Singleton.GetGrade(2)}";
				break;
			case 3:
				scoreText.text = $"{SaveManager.Singleton.GetHighScore(3)}";
				timeSurvivedText.text = $"{SaveManager.Singleton.GetTimeSurvived(3)}";
				highestStreakText.text = $"{SaveManager.Singleton.GetHighestStreak(3)}";
				gradeText.text = $"{SaveManager.Singleton.GetGrade(3)}";
				break;
		}
		//scoreboardCard.SetActive(true);
	}

	private void HideScoreboard()
	{
		//scoreboardCard.SetActive(false);
	}
}

