using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningSpawner : MonoBehaviour
{
	[Header("Waypoints")]
	[SerializeField] private List<Transform> waypoints;

	[Header("Lightning Settings")]
	[SerializeField] private GameObject lightningPrefab;
	[SerializeField] private GameObject targetAuraPrefab;
	[SerializeField] private float strikeDelay = 2f;
	[SerializeField] private float damageRadius = 5f;
	[SerializeField] private float damageAmount = 50f;
	[SerializeField] private LayerMask damageLayerMask;
	[SerializeField] private LayerMask groundLayerMask;
	[SerializeField] private int numberOfRandomStrikes = 3;
	[SerializeField] private float groundOffset = 0.25f;

	private Transform player;

	private void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	public void SpawnLightning()
	{
		numberOfRandomStrikes = Mathf.Clamp(numberOfRandomStrikes, 0, waypoints.Count);

		List<int> chosenIndices = new List<int>();
		while (chosenIndices.Count < numberOfRandomStrikes)
		{
			int randomIndex = Random.Range(0, waypoints.Count);
			if (!chosenIndices.Contains(randomIndex))
			{
				chosenIndices.Add(randomIndex);
			}
		}

		foreach (int index in chosenIndices)
		{
			Vector3 randomPosition = waypoints[index].position;
			randomPosition = GetGroundPosition(randomPosition);
			GameObject randomRedAura = Instantiate(targetAuraPrefab, randomPosition, Quaternion.identity);
			StartCoroutine(StrikeLightning(randomRedAura, randomPosition));
		}

		Vector3 playerPosition = GetGroundPosition(player.position);
		GameObject playerRedAura = Instantiate(targetAuraPrefab, playerPosition, Quaternion.identity);
		StartCoroutine(StrikeLightning(playerRedAura, playerPosition));
	}

	private Vector3 GetGroundPosition(Vector3 position)
	{
		RaycastHit hit;
		if (Physics.Raycast(position + Vector3.up * 10, Vector3.down, out hit, Mathf.Infinity, groundLayerMask))
		{
			return hit.point + Vector3.up * groundOffset;
		}
		return position;
	}

	private IEnumerator StrikeLightning(GameObject redAura, Vector3 strikePosition)
	{
		yield return new WaitForSeconds(strikeDelay);

		Destroy(redAura);
		Instantiate(lightningPrefab, strikePosition, Quaternion.identity);

		ApplyDamage(strikePosition, damageRadius);
	}

	private void ApplyDamage(Vector3 center, float radius)
	{
		Collider[] hitColliders = Physics.OverlapSphere(center, radius, damageLayerMask);

		foreach (Collider hitCollider in hitColliders)
		{
			Player player = hitCollider.GetComponent<Player>();
			if (player != null)
			{
				AchievementManager.Instance.UnlockAchievement("kachow");
				player.TakeDamage(damageAmount);
			}
			Enemy enemy = hitCollider.GetComponent<Enemy>();
			if (enemy != null)
			{
				enemy.TakeDamage(damageAmount);
			}
		}
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		foreach (Transform waypoint in waypoints)
		{
			Gizmos.DrawWireSphere(waypoint.position, damageRadius);
		}
	}
}
