using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
[CustomEditor(typeof(Achievement))]
public class AchievementEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		Achievement achievement = (Achievement)target;

		if (achievement.icon != null)
		{
			GUILayout.Label("Sprite Preview:", EditorStyles.boldLabel);
			Rect rect = GUILayoutUtility.GetRect(0, 0, 64, 64);
			rect.width = EditorGUIUtility.currentViewWidth;
			GUI.DrawTexture(rect, achievement.icon.texture, ScaleMode.ScaleToFit);
		}
	}
}
#endif
[CreateAssetMenu(fileName = "Achievement", menuName = "Achievement/New Achievement")]
public class Achievement : ScriptableObject
{
	public Sprite icon;
	public string achievementID;
	public string title;
	public string description;
	public bool isUnlocked;
	public int currentProgress;
	public int goal;

	public void Unlock()
	{
		isUnlocked = true;
		Debug.Log($"Achievement Unlocked: {title}");
	}

	public void IncrementProgress(int amount)
	{
		if (!isUnlocked)
		{
			currentProgress += amount;
			if (currentProgress >= goal)
			{
				Unlock();
			}
		}
	}
}

