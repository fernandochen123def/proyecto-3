using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToTutorial : MonoBehaviour
{
	// Start is called before the first frame update
	void Start()
	{
		SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.TutorialScene }, true);

	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
