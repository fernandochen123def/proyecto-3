using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MirrorZoom : MonoBehaviour
{
	public Transform mirrorParent;
	public Transform[] desiredPositions;

	public Transform cam;

	// Start is called before the first frame update
	void Start()
	{

		Sequence mirrorZoom = DOTween.Sequence();
		mirrorZoom.Append(cam.transform.DOPunchPosition(Vector3.up / 2, .2f, 20, 1, false));


			for (int j = 0; j < mirrorParent.childCount; j++)
			{
				Transform mirrorPiece = mirrorParent.GetChild(j);

				mirrorZoom.Join(mirrorPiece.DOLocalRotate(new Vector3(Random.Range(0, 30), Random.Range(0, 40), Random.Range(0, 30)), 2f));
				mirrorZoom.Join(mirrorPiece.DOScale(mirrorPiece.localScale / 1.1f, .2f));
			}


		mirrorZoom.Append(mirrorParent.DOMoveZ(-400, .3f));
		mirrorZoom.Join(mirrorParent.DOBlendableRotateBy(Vector3.right * 30, .3f));


	}
}

