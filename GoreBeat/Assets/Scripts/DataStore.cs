using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataStore
{
	private static int punchIndex;

	private static int levelIndex;

	private static bool isInMainMenu;

	private static bool experimentalMode;

	public static int PunchIndex {  get { return punchIndex; } set { punchIndex = value; } }
	public static int LevelIndex {  get { return levelIndex; } set { levelIndex = value; } }
	public static bool IsInMainMenu {  get { return isInMainMenu; } set { isInMainMenu = value; } }
	public static bool ExperimentalMode {  get { return experimentalMode; } set { experimentalMode = value; } }
    
}
