using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class SceneLoaderComplex : MonoBehaviour
{
	private static SceneLoaderComplex instance;
	public static SceneLoaderComplex Instance => instance;
	[SerializeField] private string sceneLoaderComplex = "SceneLoaderSimple";
	[SerializeField] private string mainMenuScene = "GameplayMainMenu";
	[SerializeField] private string introScene = "Intro";
	[SerializeField] private string tutorialScene = "LevelTutorial";
	[SerializeField] private string gameplayScene = "Gameplay";
	[SerializeField] private string firstLevel = "Level 1";
	[SerializeField] private string secondLevel = "Level 2";
	[SerializeField] private string thirdLevel = "Level 3";
	[SerializeField] private string winScene = "WinScene";
	[SerializeField] private string calibrator = "Calibrador";
	[SerializeField] private string scoreBorad = "ScoreBoard";
	//[SerializeField] private EventSystem eventSystem;

	public string MainMenuScene
	{
		get
		{
			return mainMenuScene;
		}
	}
	public string IntroScene
	{
		get
		{
			return introScene;
		}
	}
	public string TutorialScene
	{
		get
		{
			return tutorialScene;
		}
	}
	public string GameplayScene
	{
		get { return gameplayScene; }
	}
	public string WinScene
	{
		get { return winScene; }
	}
	public string FirstLevel
	{
		get { return firstLevel; }
	}
	public string SecondLevel
	{
		get { return secondLevel; }
	}
	public string ThirdLevel
	{
		get { return thirdLevel; }
	}
	public string Calibrador
	{
		get { return calibrator; }
	}
	public string ScoreBoard
	{
		get { return scoreBorad; }
	}
	// Start is called before the first frame update
	private void Awake()
	{


		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(this);
			return;
		}

		LoadScenes(new string[] { mainMenuScene }, true);

	}


	public void LoadScenes(string[] scenesNames, bool removeOtherScenes)
	{
		//eventSystem.enabled = false;
		//Load All wanted Scene.
		if (removeOtherScenes)
		{
			StartCoroutine(RemoveUnwantedScenes(scenesNames));
		}
		for (int i = 0; i < scenesNames.Length; i++)
		{
			LoadScene(scenesNames[i]);
		}


	}

	public void LoadScene(string sceneName)
	{
		if (IsSceneLoaded(sceneName))
		{
			//Debug.LogFormat("Scene <color=yellow>" + sceneName + "</color> is Already loaded, not loading anything");
		}
		else
		{
			SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
		}
	}

	private IEnumerator RemoveUnwantedScenes(string[] wantedScenes)
	{
		yield return null; //Needs at least 1 frame to ensure that the scene is loaded in the editor.

		List<string> unwantedScenes = new List<string>();

		for (int i = 0; i < SceneManager.sceneCount; i++)
		{
			bool removeScene = true;

			for (int j = 0; j < wantedScenes.Length; j++)
			{
				if (SceneManager.GetSceneAt(i).name == wantedScenes[j])
				{
					removeScene = false;
				}
			}
			if (removeScene)
			{
				unwantedScenes.Add(SceneManager.GetSceneAt(i).name);
				removeScene = false;
			}
		}

		for (int i = 0; i < unwantedScenes.Count; i++)
		{
			if (SceneManager.GetSceneAt(i).name != sceneLoaderComplex)
			{
				//The static scene will be ignored always, not possible to unload it. -> Debug.LogFormat("Removing Scene <color=orange>" + SceneManager.GetSceneAt(i).name + "</color>");
				SceneManager.UnloadSceneAsync(unwantedScenes[i]);
			}
		}
	}

	public bool IsSceneLoaded(string sceneName)
	{
		for (int i = 0; i < SceneManager.sceneCount; i++)
		{
			if (sceneName == SceneManager.GetSceneAt(i).name)
			{
				return true;
			}
		}
		return false;
	}

}
