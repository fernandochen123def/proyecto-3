public class GradeCalculator
{
	private readonly int sPlusThreshold;
	private readonly int sThreshold;
	private readonly int aThreshold;
	private readonly int bThreshold;
	private readonly int cThreshold;
	private readonly int dThreshold;

	public GradeCalculator(int sPlus, int s, int a, int b, int c, int d)
	{
		sPlusThreshold = sPlus;
		sThreshold = s;
		aThreshold = a;
		bThreshold = b;
		cThreshold = c;
		dThreshold = d;
	}

	public string CalculateGrade(int score)
	{
		if (score >= sPlusThreshold)
		{
			return "S+";
		}
		else if (score >= sThreshold)
		{
			return "S";
		}
		else if (score >= aThreshold)
		{
			return "A";
		}
		else if (score >= bThreshold)
		{
			return "B";
		}
		else if (score >= cThreshold)
		{
			return "C";
		}
		else if (score >= dThreshold)
		{
			return "D";
		}
		else
		{
			return "F";
		}
	}
}
