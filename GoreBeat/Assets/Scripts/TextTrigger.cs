using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;

public class TextTrigger : MonoBehaviour
{
	[SerializeField] private bool haveEntered;
	[SerializeField] private TextMeshProUGUI textSpawner;
	[SerializeField] private string text;
	[SerializeField] private UnityEvent onTriggerStay;
	[SerializeField] private UnityEvent onTriggerExit;
	[SerializeField] private string tagFilter;

	private void Start()
	{
		
	}
	private void OnTriggerStay(Collider other)
	{
		if (!String.IsNullOrEmpty(tagFilter) && !other.gameObject.CompareTag(tagFilter)) return;
		haveEntered = true;
		if (haveEntered)
		{
			onTriggerStay.Invoke();
			textSpawner.text = text;
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if (!String.IsNullOrEmpty(tagFilter) && !other.gameObject.CompareTag(tagFilter)) return;

		onTriggerExit.Invoke();
		haveEntered = false;
		textSpawner.text = null;


	}
}
