using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.Playables;
using UnityEngine.UI;

public class MainMenuControl : MonoBehaviour
{
	[SerializeField] GameObject firtsSelectedInMainMenuButtons;
	[SerializeField] GameObject firtsSelectedInOptionButtons;

	[Header("Timeline Animation")]
	[SerializeField] public PlayableDirector openOptionsAnimations;
	[SerializeField] public PlayableDirector openMenuAnimations;
	[SerializeField] public PlayableDirector playGameAnimations;
	[SerializeField] public PlayableDirector SelectLevelAnimations;
	[SerializeField] public PlayableDirector BackToMenuFromSelectLevelAnimations;
	[SerializeField] public PlayableDirector SelectGunsAnimations;
	[SerializeField] public PlayableDirector BackToSelectedLevelFromSelectGunsAnimations;
	[SerializeField] public PlayableDirector OpenArchivementAnimation;
	[SerializeField] public PlayableDirector backArchivementAnimation;

	[Header("Scenes")]
	[SerializeField] public GameObject mainMenu;
	[SerializeField] public GameObject optionMenu;
	[SerializeField] public GameObject selectLevelMenu;
	[SerializeField] public GameObject selectPunchMenu;
	[SerializeField] public GameObject archivementMenu;
	[SerializeField] public GameObject fade;

	[Header("Bool")]
	[SerializeField] public int LevelSelected;
	[SerializeField] public bool isInOption;
	[SerializeField] public bool InLevelSelector;
	[SerializeField] public bool InPunchSelector;
	[SerializeField] public bool PunchSelected;
	[SerializeField] public bool archivementSelector;
	[SerializeField] public bool experimentalMode;
	[SerializeField] public bool inMainMenu;

	[Header("Buttons")]
	[SerializeField] public Button firstLevelButton;
	[SerializeField] public Button secondLevelButton;
	[SerializeField] public Button thirdLevelButton;
	[SerializeField] public Toggle experimentalChange;
	[SerializeField] public GameObject firstLevelLocker;
	[SerializeField] public GameObject secondLevelLocker;
	[SerializeField] public GameObject thirdLevelLocker;
	[SerializeField] public GameObject secondPunchLocker;
	[SerializeField] public GameObject thirdPunchLocker;

	[Header("SFX")]
	[SerializeField] public AudioSource clickButton;

	ProgressionSetter progressionSetter;

	private void OnEnable()
	{
		DataStore.IsInMainMenu = true;
	}
	private void OnDisable()
	{
		DataStore.IsInMainMenu = false;
	}
	private void Awake()
	{
		bool experimentalMode = experimentalChange.isOn;
		DataStore.ExperimentalMode = experimentalMode;
		fade.SetActive(true);
		mainMenu.SetActive(true);
		optionMenu.SetActive(false);
		selectLevelMenu.SetActive(false);
		selectPunchMenu.SetActive(false);
		archivementMenu.SetActive(false);
		InPunchSelector = false;
		isInOption = false;
		InLevelSelector = false;
	}
	private void Start()
	{
		DataStore.IsInMainMenu = true;
		DataStore.PunchIndex = 0;
		progressionSetter = FindAnyObjectByType<ProgressionSetter>();

	}
	private void LateUpdate()
	{
		LockersLevelsAndPunch();

		
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (inMainMenu)
			{
				return;
			}
			if (isInOption)
			{
				inMainMenu = true;
				BackMenuAnimation();
				isInOption = false;
				archivementSelector = false;

			}
			if (InLevelSelector)
			{
				inMainMenu = true;
				LevelToMenuAnimation();
				InLevelSelector = false;
				InPunchSelector = false;
				archivementSelector = false;

			}
			if (InPunchSelector)
			{
				CloseGunsAnimation();
				InPunchSelector = false;
				InLevelSelector = true;
				archivementSelector = false;

			}
			if (archivementSelector)
			{
				CloseArchivement();
				archivementSelector = false;
				inMainMenu = true;
				isInOption = false;


			}
		}
		if (InPunchSelector)
		{
			if (PunchSelected)
			{
				DataStore.LevelIndex = LevelSelected;

				switch (LevelSelected)
				{
					case 0:
						DataStore.IsInMainMenu = false;
						SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.TutorialScene }, true);

						break;
					case 1:
						DataStore.IsInMainMenu = false;
						SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.FirstLevel }, true);

						break;
					case 2:

						DataStore.IsInMainMenu = false;
						SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.SecondLevel }, true);

						break;
					case 3:
						DataStore.IsInMainMenu = false;
						SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.ThirdLevel }, true);

						break;
				}
			}
		}
	}
	public void IsExperimentalModeActive()
	{
		experimentalMode = experimentalChange.isOn;
		DataStore.ExperimentalMode = experimentalMode;


	}
	public void UnlockAll()
	{
		SaveManager.Singleton.UnlockAll();
		progressionSetter.UpdateButtonInteractability();
	}
	public void ResetAll()
	{
		SaveManager.Singleton.ResetAll();
		progressionSetter.UpdateButtonInteractability();

	}

	public void LockersLevelsAndPunch()
	{
		SaveManager.Singleton.Load();
		bool isFirstLevelUnlock = SaveManager.Singleton.IsFirstLevelUnlocked();
		bool isSecondLevelUnlock = SaveManager.Singleton.IsSecondLevelUnlocked();
		bool isThirdLevelUnlock = SaveManager.Singleton.IsThirdLevelUnlocked();
		bool isSecondFirstUnlock = SaveManager.Singleton.IsSecondFistUnlocked();
		bool isThirdFirstUnlock = SaveManager.Singleton.IsThirdFistUnlocked();
		if (isFirstLevelUnlock)
		{
			firstLevelLocker.SetActive(false);
		}
		else
		{
			firstLevelLocker.SetActive(true);

		}
		if (isSecondLevelUnlock)
		{
			secondLevelLocker.SetActive(false);
		}
		else
		{
			secondLevelLocker.SetActive(true);

		}
		if (isThirdLevelUnlock)
		{
			thirdLevelLocker.SetActive(false);
		}
		else
		{
			thirdLevelLocker.SetActive(true);
		}
		if (isSecondFirstUnlock)
		{
			secondPunchLocker.SetActive(false);
		}
		else
		{
			secondPunchLocker.SetActive(true);

		}
		if (isThirdFirstUnlock)
		{
			thirdPunchLocker.SetActive(false);

		}
		else
		{
			thirdPunchLocker.SetActive(true);

		}
	}
	public void OpenArchivement()
	{
		OpenArchivementAnimation.Play();
		archivementSelector = true;
		inMainMenu = false;


	}
	public void CloseArchivement()
	{
		backArchivementAnimation.Play();

	}

	public void StartGame()
	{
		DataStore.LevelIndex = 0;
		playGameAnimations.Play();
		DataStore.IsInMainMenu = false;

	}
	public void ClickSound()
	{
		clickButton.Play();
	}
	public void OptionAnimation()
	{
		isInOption = true;
		openOptionsAnimations.Play();
		inMainMenu = false;

	}
	public void BackMenuAnimation()
	{

		openMenuAnimations.Play();
		isInOption = false;

	}

	public void NewGameAnimation()
	{

		playGameAnimations.Play();
		inMainMenu = false;

	}
	public void SelectLevelAnimation()
	{

		SelectLevelAnimations.Play();
		InLevelSelector = true;
		inMainMenu = false;

	}
	public void LevelToMenuAnimation()
	{

		BackToMenuFromSelectLevelAnimations.Play();
	}
	public void OpenGunsAnimation()
	{

		InLevelSelector = false;

		InPunchSelector = true;
		SelectGunsAnimations.Play();
		inMainMenu = false;


	}
	public void CloseGunsAnimation()
	{
		Debug.Log("backGuns");
		InPunchSelector = false;
		InLevelSelector = true;
		PunchSelected = false;
		inMainMenu = false;


		BackToSelectedLevelFromSelectGunsAnimations.Play();

	}
	public void TutorialButtonLevelSelected()
	{
		LevelSelected = 0;
	}
	public void FirstButtonLevelSelected()
	{

		LevelSelected = 1;
	}
	public void SecondButtonLevelSelected()
	{

		LevelSelected = 2;

	}
	public void ThirdButtonLevelSelected()
	{

		LevelSelected = 3;

	}
	public void FirstButtonPunchSelected()
	{
		DataStore.PunchIndex = 1;
		PunchSelected = true;

	}
	public void SecondButtonPunchSelectedl()
	{
		DataStore.PunchIndex = 2;
		PunchSelected = true;

	}
	public void ThirdButtonPunchSelected()
	{
		DataStore.PunchIndex = 3;
		PunchSelected = true;

	}
	public void ExitAplication()
	{
		Application.Quit();
	}

}

