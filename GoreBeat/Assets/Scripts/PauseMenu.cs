using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.Playables;
using static UnityEngine.InputSystem.InputSettings;
using UnityEngine.InputSystem.LowLevel;

public class PauseMenu : MonoBehaviour
{
	[Header("Pause Object")]
	[SerializeField] public GameObject pauseUI;
	[SerializeField] public GameObject settingUI;
	[SerializeField] public GameObject textSpawner;
	[SerializeField] public bool isInMainMenu;
	[SerializeField] public Slider MusicVolumeSlider;
	[SerializeField] public Slider FXVolumeSlider;
	[SerializeField] public Slider MasterMusic;
	[Header("Variable")]
	[SerializeField] public bool isPaused;
	[SerializeField] public bool isSetting;
	[SerializeField] public bool isExit;
	[SerializeField] public bool canPulsedExit;
	[Header("TimeLine")]
	[SerializeField] public PlayableDirector openPauseMenu;
	[SerializeField] public PlayableDirector closePauseMenu;
	[SerializeField] public PlayableDirector openSettingMenu;
	[SerializeField] public PlayableDirector closeSettingMenu;
	[SerializeField] public PlayableDirector restartAnim;
	[SerializeField] public PlayableDirector goMenuAnim;
	[SerializeField] public PlayableDirector openExitAnim;
	[SerializeField] public PlayableDirector closeExitAnim;

	[SerializeField] public AudioSource track;

	public AudioMixerSnapshot paused;
	public AudioMixerSnapshot unpaused;

	Canvas canvas;
	public bool IsPause {  get { return isPaused; }set { value = isPaused; } }
	// Start is called before the first frame update
	private void Awake()
	{
		isInMainMenu = DataStore.IsInMainMenu;

		if (!isInMainMenu)
		{
			pauseUI.SetActive(false);
			settingUI.SetActive(false);


		}
		else
		{
			pauseUI = null;
			settingUI = null;
			textSpawner = null;

		}
		
	}
	void Start()
    {
		canPulsedExit = true;
		isInMainMenu = DataStore.IsInMainMenu;
		canvas = GetComponent<Canvas>();
		LoadState();
		if (!isInMainMenu)
		{
			pauseUI.SetActive(false);
			settingUI.SetActive(false);
		}
		else
		{
			pauseUI = null;
			settingUI = null;
		}
    }

    // Update is called once per frame
    void Update()
    {
		if (!isInMainMenu)
		{
			if (canPulsedExit && Input.GetKeyDown(KeyCode.Escape))
			{
				StartCoroutine(WaitForClickPause());

				//if (isExit && !isSetting)
				//{
				//	CloseExit();
				//	isExit = false;

				//}
				//if (isSetting && !isExit)
				//{
				//	BackToMain();
				//	isSetting = false;
				//}
				if (isPaused && !isSetting && !isExit)
				{
					Resume();
					isPaused = false;
				}
				else if (!isPaused && !isSetting && !isExit)
				{
					PauseGame();
					isPaused = true;
				}
				
			}
		}
		else
		{
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;

		}
	}
	private void LateUpdate()
	{
		if (!isInMainMenu)
		{
			if (isPaused)
			{
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}
			else
			{
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
			}
		}
		

	}
	void LoadState()
	{
		MusicVolumeSlider.value = PlayerPrefs.GetFloat("MusicVolume", 0);
		FXVolumeSlider.value = PlayerPrefs.GetFloat("FXVolume", 0);
		MasterMusic.value = PlayerPrefs.GetFloat("MasterVolume", 0);
	}
	public void SaveState()
	{
		PlayerPrefs.SetFloat("MusicVolume", MusicVolumeSlider.value);
		PlayerPrefs.SetFloat("FXVolume", FXVolumeSlider.value);
		PlayerPrefs.SetFloat("MasterVolume", MasterMusic.value);
	}

	public void Resume()
	{

		double durationPlayableDirector = closePauseMenu.duration;

		StartCoroutine(WaitForCloseMenu());
	}
	public void PauseGame()
	{
		textSpawner.SetActive(false);
		pauseUI.SetActive(true);
		openPauseMenu.Play();
		Time.timeScale = 0f;
		isPaused = true;
		isExit = false;
		isSetting = false;
		track.Pause();
		paused.TransitionTo(0.01f);
		Cursor.lockState = isPaused ? CursorLockMode.Locked : CursorLockMode.None;

	}
	public void SettingGame()
	{

		settingUI.SetActive(true);

		isSetting = true;
		openSettingMenu.Play();
		settingUI.SetActive(true);
		pauseUI.SetActive(false);

	}
	public void Restart()
	{
		SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.ScoreBoard }, true);

	}
	public void BackToMain()
	{

		isSetting = false;
		closeSettingMenu.Play();

	}
	public void ExitGame()
	{


		settingUI.SetActive(false);
		pauseUI.SetActive(true);

	}
	public void OpenExit()
	{

		isExit = true;
		isSetting = false;

		openExitAnim.Play();
	}
	public void CloseExit()
	{

		isExit = false;
		closeExitAnim.Play();
	}
	public void BackToMainMennu()
	{
		unpaused.TransitionTo(0.01f);
		Time.timeScale = 1f;
		SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.MainMenuScene }, true);
	}
	#region Coroutines
	IEnumerator WaitForCloseMenu()
	{
		closePauseMenu.Play();
		yield return new WaitForSecondsRealtime(1f);
		pauseUI.SetActive(false);

		textSpawner.SetActive(true);

		Time.timeScale = 1.0f;
		isPaused = false;
		track.UnPause();
		SaveState();
		unpaused.TransitionTo(0.01f);

	}
	IEnumerator WaitForClickPause()
	{
		canPulsedExit = false;
		yield return new WaitForSecondsRealtime(1f);
		canPulsedExit = true;
	}
	#endregion
}
