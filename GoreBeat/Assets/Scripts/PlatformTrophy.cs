using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTrophy : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		AchievementManager.Instance.UnlockAchievement("platform");
	}
}
