using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Playables;

public class ScoreSceneScript : MonoBehaviour
{
	private int LevelSelected;
	[Header("Buttons")]
	[SerializeField] private GameObject playAgainButton;
	[SerializeField] private GameObject mainMenuButton;
	[Header("PlayableDirector")]
	[SerializeField] public PlayableDirector mainMenuTime;
	[Header("ScoreCard")]
	[SerializeField] private TextMeshProUGUI scoreText;
	[SerializeField] private TextMeshProUGUI timeSurvivedText;
	[SerializeField] private TextMeshProUGUI highestStreakText;
	[SerializeField] private TextMeshProUGUI gradeText;

	void Start()
    {
		LevelSelected = DataStore.LevelIndex;
		playAgainButton.SetActive(false);
		mainMenuButton.SetActive(false);
		scoreText.text = $"{ScoreData.Score}";
		timeSurvivedText.text = $"{ScoreData.TimeSurvived}";
		gradeText.text = $"{ScoreData.Grade}";
		highestStreakText.text = $"{ScoreData.HighestStreak}";
	}

    void Update()
    {
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
	}
	public void PlayAgainButton()
	{
		switch (LevelSelected)
		{
			case 0:
				SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.TutorialScene }, true);

				break;
			case 1:
				SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.FirstLevel }, true);

				break;
			case 2:
				SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.SecondLevel }, true);

				break;
			case 3:
				SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.ThirdLevel }, true);

				break;
		}
	}
	public void BackToMainMenu()
	{
		mainMenuTime.Play();
		StartCoroutine(WaitForTimeLine());
	}

	IEnumerator WaitForTimeLine()
	{
		yield return new WaitForSeconds(1f);
		SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.MainMenuScene }, true);

	}
}
