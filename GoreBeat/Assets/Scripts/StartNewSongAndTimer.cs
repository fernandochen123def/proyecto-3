using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartNewSongAndTimer : MonoBehaviour
{
	bool started = false;
	[SerializeField] AudioClip newTrack;
	[SerializeField] int trackBPM;
	[SerializeField] AudioSource trackAudioSource;
	[SerializeField] GameObject timer;
	BeatArrows beatSystem;
	private void Awake()
	{
		beatSystem = FindObjectOfType<BeatArrows>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (started) { return; }
		if (other.CompareTag("Player")) 
		{
			started = true;
			beatSystem.bpm = trackBPM;
			trackAudioSource.clip = newTrack;
			trackAudioSource.Play();
			timer.SetActive(true);
		}
	}
}
