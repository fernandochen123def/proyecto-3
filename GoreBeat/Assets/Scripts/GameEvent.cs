using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
	public void OnTutorialCompleted()
	{
		AchievementManager.Instance.UnlockAchievement("Kill");
	}
}
