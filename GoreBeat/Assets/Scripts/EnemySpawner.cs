using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	[Header("Activation")]
	public bool running = true;

	[Header("Enemy Prefabs")]
	[SerializeField] private GameObject basicEnemyPrefab;
	[SerializeField] private GameObject explosiveEnemyPrefab;
	[SerializeField] private GameObject distanceEnemyPrefab;

	[Header("Spawn Settings")]
	[SerializeField] private List<Transform> spawnPoints;
	[SerializeField] private float safeDistanceFromPlayer = 5f;
	[SerializeField] private Transform player;

	public void SpawnBasicEnemy()
	{
		if (!running) { return; }
		Transform spawnPoint = GetSafeSpawnPoint();
		if (spawnPoint != null)
		{
			Instantiate(basicEnemyPrefab, spawnPoint.position, Quaternion.identity);
		}
	}

	public void SpawnExplosiveEnemy()
	{
		if (!running) { return; }
		Transform spawnPoint = GetSafeSpawnPoint();
		if (spawnPoint != null)
		{
			Instantiate(explosiveEnemyPrefab, spawnPoint.position, Quaternion.identity);
		}
	}

	public void SpawnDistanceEnemy()
	{
		if (!running) { return; }
		Transform spawnPoint = GetSafeSpawnPoint();
		if (spawnPoint != null)
		{
			Instantiate(distanceEnemyPrefab, spawnPoint.position, Quaternion.identity);
		}
	}

	private Transform GetSafeSpawnPoint()
	{
		Transform closestSafeSpawnPoint = null;
		float closestDistance = float.MaxValue;

		foreach (Transform spawnPoint in spawnPoints)
		{
			float distanceToPlayer = Vector3.Distance(spawnPoint.position, player.position);
			if (distanceToPlayer > safeDistanceFromPlayer && distanceToPlayer < closestDistance)
			{
				closestDistance = distanceToPlayer;
				closestSafeSpawnPoint = spawnPoint;
			}
		}

		return closestSafeSpawnPoint;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(player.position, safeDistanceFromPlayer);
	}
}
