using UnityEngine;
using UnityEngine.Audio;
using TMPro;
using System;
using StarterAssets;
using System.Collections;
using PampelGames.GoreSimulator;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
	[Header("Animator")]
	[SerializeField] public Animator leftDefaultAnimator;
	[SerializeField] public Animator rightDefaultAnimator;
	[SerializeField] public Animator leftFirstAnimator;
	[SerializeField] public Animator rightFirstAnimator;
	[SerializeField] public Animator leftSecondAnimator;
	[SerializeField] public Animator rightSecondAnimator;
	[SerializeField] public Animator leftThirdAnimator;
	[SerializeField] public Animator rightThirdAnimator;
	[SerializeField] private string punchAnim = "Punch";
	[SerializeField] private string hookAnim = "Hook";
	[SerializeField] private string chargeAnim = "Reloading";
	[SerializeField] private string valve1 = "FirstValve";
	[SerializeField] private string valve2 = "SecondValve";
	[SerializeField] private string valve3 = "ThirdValve";
	[SerializeField] private string valve4 = "FourthValve";
	[SerializeField] private string jumpAnim = "Jump";
	[SerializeField] private string shootAnim = "Shoot";
	[SerializeField] private string shootingAnim = "Shooting";
	[SerializeField] private string slamAnim = "PunchGround";
	[SerializeField] private string slamEndAnim = "BodyslamEnd";
	[SerializeField] private string win = "Win";

	[Header("Punch")]
	[SerializeField] public GameObject basicPunch;
	[SerializeField] public GameObject distancePunch;
	[SerializeField] public GameObject viperPunch;

	[Header("Player default Punch")]
	public float health = 100f;
	public float maxHealth = 100f;
	[SerializeField] private float damage = 10f;
	[SerializeField] private float actionBarMaxCharges = 5f;
	[SerializeField] private float chargeIncrement = 0.5f;
	[SerializeField] private float punchIncrement = 0.25f;
	[SerializeField] private LayerMask enemyLayer;
	[SerializeField] private Transform punchDirection;
	[SerializeField] private float punchRange = 3.5f;
	[SerializeField] private float slamRadius = 5f;
	[SerializeField] private float slamDamage = 100f;

	[Header("Player First Punch")]
	[SerializeField] private float healthFirst = 100f;
	[SerializeField] private float damageFirst = 10f;
	[SerializeField] private float actionBarMaxChargesFirst = 5f;
	[SerializeField] private float chargeIncrementFirst = 0.5f;
	[SerializeField] private float punchIncrementFirst = 0.25f;
	[SerializeField] private float slamRadiusFirst = 5f;
	[SerializeField] private float slamDamageFirst = 100f;


	[Header("Player Secundary Punch")]
	[SerializeField] private float healthSecond = 70f;
	[SerializeField] private float damageSecond = 7f;
	[SerializeField] private float actionBarMaxChargesSecond = 5f;
	[SerializeField] private float chargeIncrementSecond = 0.75f;
	[SerializeField] private float punchIncrementSecond = 0.45f;
	[SerializeField] private float slamRadiusSecond = 3f;
	[SerializeField] private float slamDamageSecond = 80f;

	[Header("Player Third Punch")]
	[SerializeField] private float healthThird = 85f;
	[SerializeField] private float damageThird = 15f;
	[SerializeField] private float actionBarMaxChargesThird = 5f;
	[SerializeField] private float chargeIncrementThird = 0.30f;
	[SerializeField] private float punchIncrementThird = 0.25f;
	[SerializeField] private float slamRadiusThird = 7f;
	[SerializeField] private float slamDamageThird = 100f;

	[Header("Shooting Mode")]
	[SerializeField] private GameObject projectileDefaultPrefab;
	[SerializeField] private GameObject projectileFirtsPrefab;
	[SerializeField] private GameObject projectileSecundaryPrefab;
	[SerializeField] private Transform leftDefaultShootingPoint;
	[SerializeField] private Transform rightDefaultShootingPoint;
	[SerializeField] private Transform leftFirtsShootingPoint;
	[SerializeField] private Transform rightFirtsShootingPoint;
	[SerializeField] private Transform leftSecondShootingPoint;
	[SerializeField] private Transform rightSecondShootingPoint;
	[SerializeField] private Transform leftThirdShootingPoint;
	[SerializeField] private Transform rightThirdShootingPoint;
	[SerializeField] private float projectileSpeed = 20f;
	private bool isShootingMode = false;

	[Header("UI Elements")]
	[SerializeField] private TextMeshProUGUI scoreText;
	[SerializeField] private TextMeshProUGUI streakText;
	[SerializeField] private TextMeshProUGUI multiplierText;
	[SerializeField] private TextMeshProUGUI actionBarText;
	[SerializeField] private TextMeshProUGUI healthText;
	[SerializeField] private Image healthBar;
	[SerializeField] private Image heatBar;
	[SerializeField] private BeatArrows beatArrows;
	[SerializeField] private Image restartProgressBar;
	[SerializeField] private TextMeshProUGUI restartText;

	[SerializeField] float restartDelay = 3.0f;
	private float restartTimer = 0;

	[Header("Global Volumes")]
	[SerializeField] private Volume actionBarVolume;

	[Header("VFX")]
	[SerializeField] private Material fistsMaterial;
	[SerializeField] float maxIntensity = 1f;
	private float defaultIntensity = 0f;
	[SerializeField] private Transform vfxMuzzleSpawner;
	[SerializeField] private Transform vfxDashSpawner;
	[SerializeField] private Transform vfxGroundSlamSpawner;
	[SerializeField] private GameObject vfxMuzzle;
	[SerializeField] private GameObject vfxDash;
	[SerializeField] private GameObject vfxGroundSlam;
	[SerializeField] private GameObject vfxFire;
	private GameObject vfxDestroy;

	[Header("Sounds")]
	[SerializeField] private AudioSource sfxSound;
	[SerializeField] private AudioSource narratorSound;
	[SerializeField] private AudioClip punchSound;
	[SerializeField] private AudioClip jumpSound;
	[SerializeField] private AudioClip woozSound;
	[SerializeField] private AudioClip fireSound;
	[SerializeField] private AudioClip heatMaxSound;
	[SerializeField] private AudioClip heatSound;
	[SerializeField] private AudioClip bodyslamSound;
	[SerializeField] private AudioClip valveSound;
	[SerializeField] private AudioClip soundStreak1;
	[SerializeField] private AudioClip soundStreak2;
	[SerializeField] private AudioClip soundStreak3;
	[SerializeField] private AudioClip soundStreak4;
	[SerializeField] private AudioClip soundStreak5;
	[SerializeField] private AudioClip soundKill1;
	[SerializeField] private AudioClip soundKill2;


	public enum PlayerActionState
	{
		Idle,
		Punching,
		Dashing,
		Jumping,
		Charging,
		Shooting,
		Missed
	}

	private int LevelSelected;

	private int punchIndex;
	private PlayerActionState currentActionState = PlayerActionState.Idle;
	private int fistIndex = 1;
	//private int enemiesToKillForNextMultiplier = 1;
	//private int enemiesKilled = 0;
	private float actionBarCharge = 0f;
	private float feedbackMultiplier = 1f;
	private int multiplierChain = 0;
	private int targetChain = 13;
	private bool secondChance = true;
	private int multiplier = 1;
	private int streak = 0;
	private int highestStreak = 0;
	private int score = 0;
	private float animationSpeed = 0.75f;

	[SerializeField]private CinemachineShake cinemachineShake;
	private int basicEnemiesKilled;
	private int explosiveEnemiesKilled;
	private int distanceEnemiesKilled;

	private PauseMenu pauseMenu;
	private FirstPersonController fpc;
	private Dash dash;

	private GradeCalculator tutorialGradeCalculator = new GradeCalculator(80000, 70000, 60000, 50000, 40000, 30000);
	private GradeCalculator level1GradeCalculator = new GradeCalculator(90000, 80000, 70000, 60000, 50000, 40000);
	private GradeCalculator level2GradeCalculator = new GradeCalculator(100000, 90000, 80000, 70000, 60000, 50000);
	private GradeCalculator level3GradeCalculator = new GradeCalculator(120000, 110000, 100000, 90000, 80000, 70000);

	public static Player Instance { get; private set; }
	public int HighestStreak { get => highestStreak; }
	public int Score { get => score; }

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
		}

		fpc = GetComponent<FirstPersonController>();
		dash = GetComponent<Dash>();
		pauseMenu = FindAnyObjectByType<PauseMenu>();
	}

	void Start()
	{
		LevelSelected = DataStore.LevelIndex;
		punchIndex = DataStore.PunchIndex;
		punchIndexChangeAndValues();
		ChangeShootPointAndBulletPrefab();
		UpdateUI();
		SetFistIntensity(0f);
		currentActionState = PlayerActionState.Idle;
		//GameManager.EnemyKilledEvent.AddListener(IncrementKillCount);
		sfxSound.clip = null;
	}
	void OnDestroy()
	{
		//GameManager.EnemyKilledEvent.RemoveListener(IncrementKillCount);
	}

	void Update()
	{
		if (pauseMenu.IsPause)
		{
			return;
		}

		if (Input.GetKeyDown(KeyCode.Q))
		{
			sfxSound.clip = valveSound;
			sfxSound.Play();
			isShootingMode = !isShootingMode;
			leftDefaultAnimator.SetBool(shootAnim, !leftDefaultAnimator.GetBool(shootAnim));
			rightDefaultAnimator.SetBool(shootAnim, !rightDefaultAnimator.GetBool(shootAnim));
		}

		if (Input.GetMouseButtonDown(0))
		{
			if (isShootingMode)
			{
				AttemptShoot();
			}
			else
			{
				AttemptPunch();
			}
		}
		if (Input.GetMouseButtonDown(1))
		{
			if (actionBarCharge == actionBarMaxCharges) { AttemptGroundSlam(); }
			else { AttemptCharge(); }
		}
		if (Input.GetKeyDown(KeyCode.Space))
		{
			AttemptJump();
		}
		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			AttemptDash();
		}

		UpdateUI();
		HandleRestartInput();
	}
	private void LateUpdate()
	{
		SoundWithStreak();
	}

	#region Player Values
	public void punchIndexChangeAndValues()
	{
		switch (punchIndex)
		{
			default:
				leftDefaultAnimator = leftFirstAnimator;
				rightDefaultAnimator = rightFirstAnimator;
				basicPunch.SetActive(true);
				distancePunch.SetActive(false);
				viperPunch.SetActive(false);

				health = healthFirst;
				maxHealth = healthFirst;
				damage = damageFirst;
				actionBarMaxCharges = actionBarMaxChargesFirst;
				chargeIncrement = chargeIncrementFirst;
				punchIncrement = punchIncrementFirst;
				slamRadius = slamRadiusFirst;
				slamDamage = slamDamageFirst;

				break;
			case 1:
				leftDefaultAnimator = leftFirstAnimator;
				rightDefaultAnimator = rightFirstAnimator;
				basicPunch.SetActive(true);
				distancePunch.SetActive(false);
				viperPunch.SetActive(false);

				health = healthFirst;
				maxHealth = healthFirst;
				damage = damageFirst;
				actionBarMaxCharges = actionBarMaxChargesFirst;
				chargeIncrement = chargeIncrementFirst;
				punchIncrement = punchIncrementFirst;
				slamRadius = slamRadiusFirst;
				slamDamage = slamDamageFirst;



				break;
			case 2:
				leftDefaultAnimator = leftSecondAnimator;
				rightDefaultAnimator = rightSecondAnimator;
				basicPunch.SetActive(false);
				distancePunch.SetActive(true);
				viperPunch.SetActive(false);

				health = healthSecond;
				maxHealth = healthSecond;
				damage = damageSecond;
				actionBarMaxCharges = actionBarMaxChargesSecond;
				chargeIncrement = chargeIncrementSecond;
				punchIncrement = punchIncrementSecond;
				slamRadius = slamRadiusSecond;
				slamDamage = slamDamageSecond;




				break;
			case 3:
				leftDefaultAnimator = leftThirdAnimator;
				rightDefaultAnimator = rightThirdAnimator;
				distancePunch.SetActive(false);
				basicPunch.SetActive(false);
				viperPunch.SetActive(true);

				health = healthThird;
				maxHealth = healthThird;
				damage = damageThird;
				actionBarMaxCharges = actionBarMaxChargesThird;
				chargeIncrement = chargeIncrementThird;
				punchIncrement = punchIncrementThird;
				slamRadius = slamRadiusThird;
				slamDamage = slamDamageThird;



				break;
		}
	}
	void ChangeShootPointAndBulletPrefab()
	{
		switch (punchIndex)
		{
			default:
				leftDefaultShootingPoint = leftFirtsShootingPoint;
				rightDefaultShootingPoint = rightFirtsShootingPoint;
				projectileDefaultPrefab = projectileFirtsPrefab;
				break;
			case 1:
				leftDefaultShootingPoint = leftFirtsShootingPoint;
				rightDefaultShootingPoint = rightFirtsShootingPoint;
				projectileDefaultPrefab = projectileFirtsPrefab;

				break;
			case 2:
				leftDefaultShootingPoint = leftSecondShootingPoint;
				rightDefaultShootingPoint = rightSecondShootingPoint;
				projectileDefaultPrefab = projectileSecundaryPrefab;

				break;
			case 3:
				leftDefaultShootingPoint = leftThirdShootingPoint;
				rightDefaultShootingPoint = rightThirdShootingPoint;
				projectileDefaultPrefab = projectileFirtsPrefab;

				break;
		}

	}
	#endregion
	

	private void SetAnimationSpeed(float speed)
	{
		animationSpeed = speed / beatArrows.beatInterval;
		leftDefaultAnimator.speed = animationSpeed;
		rightDefaultAnimator.speed = animationSpeed;
	}

	

	private void AttemptShoot()
	{
		if (actionBarCharge < 1 || (currentActionState != PlayerActionState.Idle) || beatArrows.actioned) { return; }
		TimingFeedback feedback = beatArrows.OnPlayerAction();
		ApplyDamageFeedback(feedback, 3);
		if (feedback != TimingFeedback.Missed)
		{
			SetAnimationSpeed(2f);
			Transform shootingPoint;
			if (fistIndex % 2 == 0)
			{
				leftDefaultAnimator.SetTrigger(shootingAnim);
				shootingPoint = leftDefaultShootingPoint;
			}
			else
			{
				rightDefaultAnimator.SetTrigger(shootingAnim);
				shootingPoint = rightDefaultShootingPoint;
			}
			sfxSound.clip = fireSound;
			sfxSound.Play();
			fistIndex++;
			Instantiate(vfxFire, shootingPoint.transform.position, Quaternion.identity);

			ShootProjectile(shootingPoint);
			actionBarCharge--;
			currentActionState = PlayerActionState.Shooting;
			StartCoroutine(ResetActionStateAfterDelay());
			IncreaseMultiplierChain();
			UpdateHeatIntensity();
			UpdateUI();
		}
		else
		{
			currentActionState = PlayerActionState.Missed;
			StartCoroutine(ResetActionStateAfterDelay());
			DecreaseMultiplierChain();
			streak = 0;
			UpdateUI();
		}
	}
	public void ActiveWinAnimation()
	{
		leftDefaultAnimator.SetBool(win, true);
		rightDefaultAnimator.SetBool(win, true);
	}
	public void DesactiveWinAnimation()
	{
		leftDefaultAnimator.SetBool(win, false);
		rightDefaultAnimator.SetBool(win, false);
	}

	private void ShootProjectile(Transform shootingPoint)
	{
		if (projectileDefaultPrefab && leftDefaultShootingPoint)
		{
			cinemachineShake.ShakeCamera();

			GameObject projectile = Instantiate(projectileDefaultPrefab, shootingPoint.position, Quaternion.identity);
			projectile.GetComponent<Bullet>().damage *= feedbackMultiplier;
			Rigidbody rb = projectile.GetComponent<Rigidbody>();
			if (rb)
			{
				Transform cameraTransform = PlayerCameraManager.PlayerCamera.transform;
				rb.velocity = cameraTransform.forward * projectileSpeed;
			}
			Destroy(projectile, 5f); //Destroy projectile after 5 seconds if no hit
		}
	}

	private void AttemptDash()
	{
		if (fpc.input.move == Vector2.zero || (currentActionState != PlayerActionState.Idle) || beatArrows.actioned) { return; }
		TimingFeedback feedback = beatArrows.OnPlayerAction();
		if (feedback != TimingFeedback.Missed)
		{
			vfxDestroy = Instantiate(vfxDash, vfxDashSpawner.transform.position, vfxDashSpawner.transform.rotation);
			vfxDestroy.transform.parent = vfxDashSpawner.transform;
			Destroy(vfxDestroy, .6f);

			SetAnimationSpeed(1.4f);
			dash.DashInput(true);
			currentActionState = PlayerActionState.Dashing;
			StartCoroutine(ResetActionStateAfterDelay());
		}
		else { currentActionState = PlayerActionState.Missed; StartCoroutine(ResetActionStateAfterDelay()); }
	}

	private void AttemptJump()
	{
		if (!fpc.Grounded) { return; } //|| (currentActionState != PlayerActionState.Idle && currentActionState != PlayerActionState.Jumping)) { return; }
									   //TimingFeedback feedback = beatArrows.OnPlayerAction();
									   //if (feedback != TimingFeedback.Missed)
									   //{
		sfxSound.clip = jumpSound;
		sfxSound.Play();
		SetAnimationSpeed(0.45f);
		fpc.JumpAndGravity(true);
		leftDefaultAnimator.SetTrigger(jumpAnim);
		rightDefaultAnimator.SetTrigger(jumpAnim);
		//currentActionState = PlayerActionState.Jumping;
		StartCoroutine(ResetActionStateAfterDelay());
		//}
	}

	public void AttemptPunch()
	{
		if (currentActionState != PlayerActionState.Idle || beatArrows.actioned) { return; }
		TimingFeedback feedback = beatArrows.OnPlayerAction();
		if (feedback != TimingFeedback.Missed)
		{
			if(actionBarCharge >= actionBarMaxCharges) { AchievementManager.Instance.UnlockAchievement("wastedpotential"); }
			SetAnimationSpeed(1f);
			sfxSound.clip = woozSound;
			sfxSound.Play();

			if (fistIndex % 2 == 0)
			{
				if (fistIndex % 3 == 0)
				{

					leftDefaultAnimator.SetTrigger(hookAnim);
				}
				else
				{

					leftDefaultAnimator.SetTrigger(punchAnim);
				}
			}
			else
			{
				if (fistIndex % 3 == 0)
				{

					rightDefaultAnimator.SetTrigger(hookAnim);
				}
				else
				{

					rightDefaultAnimator.SetTrigger(punchAnim);
				}
			}
			fistIndex++;
			currentActionState = PlayerActionState.Punching;
			StartCoroutine(ResetActionStateAfterDelay());
		}
		else { currentActionState = PlayerActionState.Missed; StartCoroutine(ResetActionStateAfterDelay()); }
		ApplyDamageFeedback(feedback, 1);
	}

	public void AttemptCharge()
	{
		if (isShootingMode || currentActionState != PlayerActionState.Idle || beatArrows.actioned) { return; }
		cinemachineShake.ShakeCamera();

		TimingFeedback feedback = beatArrows.OnPlayerAction();
		ApplyDamageFeedback(feedback, 2);
	}

	public void AttemptGroundSlam()
	{
		if (fpc.Grounded) { return; }
		if (actionBarCharge >= actionBarMaxCharges)
		{
			SetAnimationSpeed(1f);
			currentActionState = PlayerActionState.Charging;

			//Ground slam animation
			leftDefaultAnimator.SetTrigger(slamAnim);
			rightDefaultAnimator.SetTrigger(slamAnim);

			
			fpc.LookDownAtGround();

			Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy"), true);


			StartCoroutine(ResetActionStateAfterDelay());
			StartCoroutine(WaitTillGrounded());
		}
	}

	

	void PerformGroundSlam()
	{
		AchievementManager.Instance.UnlockAchievement("greatassets");
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, slamRadius, enemyLayer);
		if(hitColliders.Length >= 5) { AchievementManager.Instance.UnlockAchievement("beatkakke"); }
		foreach (var hitCollider in hitColliders)
		{
			Enemy enemy = hitCollider.GetComponentInParent<Enemy>();
			if (enemy != null)
			{
				feedbackMultiplier = 1f; // In order to receive base score rewards
				enemy.TakeDamage(slamDamage);
			}
		}
	}

	private void UpdateUI()
	{
		scoreText.text = $"{Score}";
		streakText.text = $"{streak}";
		multiplierText.text = $"X{multiplier}";
		actionBarText.text = $"{actionBarCharge}/{actionBarMaxCharges}";
		healthText.text = $"{health}";
		//healthBar.fillAmount = health/100f;
		healthBar.fillAmount = Mathf.Lerp(healthBar.fillAmount, health / maxHealth, Time.deltaTime * 5f);
		//heatBar.fillAmount = actionBarCharge / actionBarMaxCharges;
		heatBar.fillAmount = Mathf.Lerp(heatBar.fillAmount, actionBarCharge / actionBarMaxCharges, Time.deltaTime * 5f);
		actionBarVolume.weight = Mathf.Lerp(actionBarVolume.weight, actionBarCharge / actionBarMaxCharges, Time.deltaTime * 5f);
	}
	

	void UpdateHeatIntensity()
	{
		float intensity = Mathf.Lerp(defaultIntensity, maxIntensity, Mathf.Clamp01(actionBarCharge / actionBarMaxCharges));
		SetFistIntensity(intensity);
		ValvesAnimation();
		
	}

	private void ValvesAnimation()
	{
		switch ((int)actionBarCharge)
		{
			case 1:
				leftDefaultAnimator.SetTrigger(valve1);
				rightDefaultAnimator.SetTrigger("Missed");
				break;
			case 2:
				leftDefaultAnimator.SetTrigger(valve1);
				rightDefaultAnimator.SetTrigger(valve1);
				break;
			case 3:
				leftDefaultAnimator.SetTrigger(valve2);
				rightDefaultAnimator.SetTrigger(valve1);
				break;
			case 4:
				leftDefaultAnimator.SetTrigger(valve2);
				rightDefaultAnimator.SetTrigger(valve2);
				break;
			case 5:
				leftDefaultAnimator.SetTrigger(valve3);
				rightDefaultAnimator.SetTrigger(valve2);
				break;
			case 6:
				leftDefaultAnimator.SetTrigger(valve3);
				rightDefaultAnimator.SetTrigger(valve3);
				break;
			case 7:
				//sfxSound.clip = valveSound;
				//sfxSound.Play();
				leftDefaultAnimator.SetTrigger(valve4);
				rightDefaultAnimator.SetTrigger(valve3);
				break;
			case 8:
				//sfxSound.clip = valveSound;
				//sfxSound.Play();
				leftDefaultAnimator.SetTrigger(valve4);
				rightDefaultAnimator.SetTrigger(valve4);
				break;
			default:
				leftDefaultAnimator.SetTrigger("Missed");
				rightDefaultAnimator.SetTrigger("Missed");
				break;
		}
	}

	

	private void SetFistIntensity(float intensity)
	{
		fistsMaterial.SetFloat("_Intensity", intensity);
	}

	private void ApplyDamageFeedback(TimingFeedback feedback, int damageType)
	{
		switch (feedback)
		{
			case TimingFeedback.Perfect:
				feedbackMultiplier = 2f;
				break;
			case TimingFeedback.Good:
				feedbackMultiplier = 1f;
				break;
			case TimingFeedback.Decent:
				feedbackMultiplier = 0.5f;
				break;
			case TimingFeedback.Missed:
				feedbackMultiplier = 0f;
				streak = 0;
				ResetIntensity();
				break;
		}
		if (damageType == 1) { Punch(feedback); }
		else if (damageType == 2) { ChargeActionBar(feedback); }
		if (streak > highestStreak)
		{
			highestStreak = streak;
		}
		UpdateHeatIntensity();
	}

	private void Punch(TimingFeedback feedback)
	{
		Debug.DrawRay(punchDirection.position, punchDirection.forward * punchRange, Color.red, 2f);
		bool enemyInRange = Physics.Raycast(punchDirection.position, punchDirection.forward, out RaycastHit hit, punchRange, enemyLayer);

		if (enemyInRange)
		{
			Enemy enemy = hit.collider.GetComponentInParent<Enemy>();
			if (enemy != null)
			{
				if (feedback != TimingFeedback.Missed)
				{
					cinemachineShake.ShakeCamera();
					sfxSound.clip = punchSound;
					sfxSound.Play();
					streak++;
					actionBarCharge += punchIncrement;
					float totalDamage = damage * feedbackMultiplier;
					enemy.TakeDamage(totalDamage);
					IncreaseMultiplierChain();
				}
				else
				{
					DecreaseMultiplierChain();
					//TakeDamage(5f);
				}
			}
		}
		actionBarCharge = Mathf.Clamp(actionBarCharge, 0f, actionBarMaxCharges);
	}

	public void IncrementKillCount(Enemy.EnemyType enemyType)
	{
		int baseScore = 0;
		switch (enemyType)
		{
			case Enemy.EnemyType.Basic:
				baseScore = 100;
				basicEnemiesKilled++;
				AchievementManager.Instance.UnlockAchievement("titan");
				break;
			case Enemy.EnemyType.Explosive:
				baseScore = 50;
				explosiveEnemiesKilled++;
				break;
			case Enemy.EnemyType.Distance:
				baseScore = 200;
				distanceEnemiesKilled++;
				break;
		}

		score += (int)(baseScore * multiplier * feedbackMultiplier);

		AchievementManager.Instance.UnlockAchievement("firstenemy");

		if (basicEnemiesKilled >= 1 && explosiveEnemiesKilled >= 1 && distanceEnemiesKilled >= 1) { AchievementManager.Instance.UnlockAchievement("exterminator"); }

		UpdateUI();
	}

	private void ChargeActionBar(TimingFeedback feedback)
	{
		if (feedback == TimingFeedback.Missed)
		{
			actionBarCharge = Mathf.Max(actionBarCharge - 1f, 0f);
			currentActionState = PlayerActionState.Missed;
			StartCoroutine(ResetActionStateAfterDelay());
			DecreaseMultiplierChain();
		}
		else
		{
			streak++;
			SetAnimationSpeed(1.5f);
			leftDefaultAnimator.SetTrigger(chargeAnim);
			rightDefaultAnimator.SetTrigger(chargeAnim);
			actionBarCharge += feedbackMultiplier * chargeIncrement;
			currentActionState = PlayerActionState.Charging;
			StartCoroutine(ResetActionStateAfterDelay());
			IncreaseMultiplierChain();
		}
		actionBarCharge = Mathf.Clamp(actionBarCharge, 0f, actionBarMaxCharges);
	}

	private void DecreaseMultiplierChain()
	{
		if (secondChance)
		{
			multiplierChain = Mathf.Max(0, multiplierChain - 3);
			secondChance = false;
		}
		else
		{
			DecreaseMultiplier();
			multiplierChain = 0;
		}

		UpdateUI();
	}

	private void IncreaseMultiplierChain()
	{
		multiplierChain++;

		if (multiplierChain >= targetChain)
		{
			multiplierChain -= targetChain;
			IncreaseMultiplier();
		}

		if (!secondChance)
		{
			secondChance = true;
		}

		if (multiplier == 8 && multiplierChain >= 10)
		{
			score += 50;
			multiplierChain -= 10;
		}

		UpdateUI();
	}

	private void IncreaseMultiplier()
	{
		if (multiplier < 8)
		{
			multiplier *= 2;
		}
	}

	private void DecreaseMultiplier()
	{
		if (multiplier > 1)
		{
			multiplier /= 2;
		}
	}

	public void IncreaseHeat(float amount)
	{
		actionBarCharge += amount;
		actionBarCharge = Mathf.Clamp(actionBarCharge, 0f, actionBarMaxCharges);
		if(actionBarCharge == actionBarMaxCharges) { AchievementManager.Instance.UnlockAchievement("heatmode"); }
		UpdateHeatIntensity();
		UpdateUI();
	}

	public void TakeDamage(float amount)
	{
		health -= amount;
		if (health <= 0f)
		{
			Die();
		}
	}

	private void Die()
	{
		health = 0f;
		AchievementManager.Instance.UnlockAchievement("playerdeath");
		UpdateUI();
		beatArrows.GameOver();
	}
	private void HandleRestartInput()
	{
		if (Input.GetKey(KeyCode.R))
		{
			restartTimer += Time.deltaTime;
			UpdateRestartUI(restartTimer / restartDelay);

			//leftFistAnimator.SetTrigger(restartLevel);
			//rightFistAnimator.SetTrigger(restartLevel);
			if (restartTimer >= restartDelay)
			{
				RestartLevel();
			}
		}
		else if (Input.GetKeyUp(KeyCode.R))
		{
			restartTimer = 0;
			UpdateRestartUI(0);
		}
	}
	private void UpdateRestartUI(float progress)
	{
		if (restartProgressBar)
		{
			restartProgressBar.gameObject.SetActive(progress > 0);
			restartProgressBar.fillAmount = progress;
		}
		if (restartText)
		{
			restartText.text = "Restarting...";
			restartText.gameObject.SetActive(progress > 0);
		}
	}

	private void RestartLevel()
	{
		//SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.ScoreBoard }, true);
	}

	public string CalculateGrade(int score, int level)
	{
		switch (level)
		{
			case 0:
				return tutorialGradeCalculator.CalculateGrade(score);
			case 1:
				return level1GradeCalculator.CalculateGrade(score);
			case 2:
				return level2GradeCalculator.CalculateGrade(score);
			case 3:
				return level3GradeCalculator.CalculateGrade(score);
			default:
				return "F";
		}
	}
	public string Grade => CalculateGrade(score, LevelSelected);

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, slamRadius);
	}
	void SoundWithStreak()
	{
		float indexSound = UnityEngine.Random.Range(1, 2);

		bool isActiveForFirtsTime = false;
		
			switch (streak)
			{
			default: isActiveForFirtsTime = false; break;
			case 0:
					isActiveForFirtsTime = false;
				break;
			case 5:
					if (!isActiveForFirtsTime)
					{
						if (indexSound == 1)
						{
							narratorSound.clip = soundKill1;
							narratorSound.Play();
						}
						else
						{
							narratorSound.clip = soundStreak2;
							narratorSound.Play();
						}
						isActiveForFirtsTime = true;
					}
					break;
			case 10:
					if (!isActiveForFirtsTime)
					{
						if (indexSound == 1)
						{
						narratorSound.clip = soundKill2;
						narratorSound.Play();
						}
						else
						{
						narratorSound.clip = soundStreak1;
						narratorSound.Play();
						}
						isActiveForFirtsTime = true;
					}
					break;
				case 15:
					if (!isActiveForFirtsTime)
					{
					Debug.Log("3 STREAK");
					if (indexSound == 1)
					{
						narratorSound.clip = soundStreak3;
						narratorSound.Play();
					}
					else
					{
						narratorSound.clip = soundStreak4;
						narratorSound.Play();
					}
					//Sound 1
					isActiveForFirtsTime = true;
					}
					break;
				case 20:
					if (!isActiveForFirtsTime)
					{
					if (indexSound == 1)
					{
						narratorSound.clip = soundKill2;
						narratorSound.Play();
					}
					else
					{
						narratorSound.clip = soundStreak5;
						narratorSound.Play();
					}
					//Sound 1
					isActiveForFirtsTime = true;
					}
					break;


			}
		
	}
	
	#region Coroutines
	IEnumerator ResetIntensity()
	{
		float duration = 1.0f;
		float currentTime = 0f;
		float startIntensity = fistsMaterial.GetFloat("_Intensity");

		while (currentTime < duration)
		{
			currentTime += Time.deltaTime;
			float newIntensity = Mathf.Lerp(startIntensity, defaultIntensity, currentTime / duration);
			SetFistIntensity(newIntensity);
			yield return null;
		}
	}
	private IEnumerator ResetActionStateAfterDelay()
	{
		float delay = 0.55f * beatArrows.beatInterval;
		yield return new WaitForSeconds(delay);
		currentActionState = PlayerActionState.Idle;
		SetAnimationSpeed(0.75f); //Idle speed
	}
	IEnumerator WaitTillGrounded()
	{
		while (!fpc.Grounded)
		{
			fpc.input.look.y = 0;
			fpc.Gravity = -45f;
			yield return null;
		}

		fpc.Gravity = -9.81f;

		PerformGroundSlam();

		Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy"), false);

		actionBarCharge = 0;
		UpdateHeatIntensity();

		// End ground slam animation
		Instantiate(vfxGroundSlam, vfxGroundSlamSpawner.position, Quaternion.identity);
		cinemachineShake.ShakeCamera();


		sfxSound.clip = bodyslamSound;
		sfxSound.Play();

		leftDefaultAnimator.SetTrigger(slamEndAnim);
		rightDefaultAnimator.SetTrigger(slamEndAnim);


		fpc.ResetCameraPosition();
	}
	#endregion

}
