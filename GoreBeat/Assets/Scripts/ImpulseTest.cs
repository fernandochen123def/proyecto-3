using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ImpulseTest : MonoBehaviour
{
	[SerializeField] Cinemachine.CinemachineImpulseSource impulseCameraShake;

	void Start()
    {
		impulseCameraShake = GetComponent<Cinemachine.CinemachineImpulseSource>();

	}

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
		{
			CameraShake();

		}
    }

	private void CameraShake()
	{
		impulseCameraShake.GenerateImpulse();
	}
}
