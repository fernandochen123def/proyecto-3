using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AchievementAnimation : MonoBehaviour
{
	Animator animator;
	[SerializeField] string active = "Active";
	[SerializeField] public Image icon;
	[SerializeField] public TextMeshProUGUI title;
	[SerializeField] public TextMeshProUGUI description;
	[SerializeField] public AudioSource winSound;

	// Start is called before the first frame update
	void Start()
    {
        animator = GetComponent<Animator>();
    }

   
	public void activeAnimation()
	{
		Debug.Log("win");
		winSound.Play();
		animator.SetBool(active, true);
	}
	public void desactiveAnimation()
	{
		animator.SetBool(active, false);

	}
}
