using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationLevel2 : MonoBehaviour
{
	[SerializeField] private Animator animatorLeftAntena;
	[SerializeField] private Animator animatorRightAntena;
	[SerializeField] private Animator animatorRock;
	[SerializeField] private GameObject textureWhereHit;
	[SerializeField] private AudioSource explosion;
	[SerializeField] private string prepare = "Prepare";
	[SerializeField] private string fire = "Fire";
	[SerializeField] private string hasFire = "HasFire";

	private void Start()
	{
		textureWhereHit.SetActive(false);
	}
	void Update()
    {
        
    }
	public void PrepareAnimation()
	{
		animatorLeftAntena.SetTrigger(prepare);
		animatorRightAntena.SetTrigger(prepare);
		animatorRock.SetTrigger(prepare);
		textureWhereHit.SetActive(true);

	}
	public void FireAnimation()
	{
		explosion.Play();
		animatorLeftAntena.SetTrigger(fire);
		animatorRightAntena.SetTrigger(fire);
		textureWhereHit.SetActive(false);
		Recover();

	}
	public void Recover()
	{
		animatorLeftAntena.SetTrigger(hasFire);
		animatorRightAntena.SetTrigger(hasFire);

	}


}
