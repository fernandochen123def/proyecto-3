using UnityEngine;
public class EnemyProjectile : MonoBehaviour
{
	[SerializeField] private float speed = 10f;
	[SerializeField] private float damage = 10f;
	[SerializeField] private GameObject vfxProjectileImpact;
	private Vector3 direction;

	public void Initialize(Vector3 shootDirection)
	{
		direction = shootDirection.normalized;
	}

	private void Start()
	{
		Destroy(gameObject, 10f);
	}

	private void Update()
	{
		transform.position += direction * speed * Time.deltaTime;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			other.GetComponent<Player>().TakeDamage(damage);
		}
		Instantiate(vfxProjectileImpact, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}
