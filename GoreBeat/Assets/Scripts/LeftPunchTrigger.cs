using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftPunchTrigger : MonoBehaviour
{
	float damage;
	bool hit = false;
    void Start()
    {
		damage = GetComponentInParent<Enemy>().attackDamage;
    }
	private void OnEnable()
	{
		hit = false;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (hit != true && other.CompareTag("Player"))
		{
			other.GetComponent<Player>().TakeDamage(damage);
			hit = true;
		}
	}
}
