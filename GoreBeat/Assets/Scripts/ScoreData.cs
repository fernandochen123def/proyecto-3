public static class ScoreData
{
	public static int Score { get; set; }
	public static string TimeSurvived { get; set; }
	public static string Grade { get; set; }
	public static int HighestStreak { get; set; }
}
