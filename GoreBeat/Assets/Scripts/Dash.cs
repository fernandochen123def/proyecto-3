using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using StarterAssets;

public class Dash : MonoBehaviour
{
	[SerializeField] Animator leftFistAnimator;
	[SerializeField] Animator rightFistAnimator;
	[SerializeField] private float dashTime = 0.075f;
	private float dashCooldown;
	private float lastDashTime;
	private float defaultMoveSpeed;

	private FirstPersonController fpc;
	private Player player;
	[SerializeField] private BeatArrows beat;
	[SerializeField] private AudioSource sfxSound;
	[SerializeField] private AudioClip dashSound;

	void Awake()
	{
		fpc = GetComponent<FirstPersonController>();
		player = GetComponent<Player>();
		defaultMoveSpeed = fpc.MoveSpeed;
	}
	private void Start()
	{
		leftFistAnimator = player.leftDefaultAnimator;
		rightFistAnimator = player.rightDefaultAnimator;
	}
	public void DashInput(bool dash = false)
	{
		dashCooldown = beat.beatInterval / 2;
		if (Time.time - lastDashTime < dashCooldown) return;
		Vector2 inputDir = fpc.input.move;
		if (dash && inputDir != Vector2.zero)
		{
			sfxSound.clip = dashSound;
			sfxSound.Play();

			lastDashTime = Time.time;
			StartCoroutine(PerformDash(dashTime));

			float horizontal = inputDir.x;
			float vertical = inputDir.y;

			leftFistAnimator.SetFloat("Horizontal", horizontal);
			leftFistAnimator.SetFloat("Vertical", vertical);
			rightFistAnimator.SetFloat("Horizontal", -horizontal);
			rightFistAnimator.SetFloat("Vertical", vertical);

			leftFistAnimator.SetTrigger("Dash");
			rightFistAnimator.SetTrigger("Dash");
		}
	}
	private IEnumerator PerformDash(float dashTime)
	{
		fpc.MoveSpeed = 75f;
		PlayDashEffects();
		yield return new WaitForSeconds(dashTime);
		fpc.MoveSpeed = defaultMoveSpeed;
	}

	private void PlayDashEffects()
	{
		//audio && camera shake/zoom
	}
}
