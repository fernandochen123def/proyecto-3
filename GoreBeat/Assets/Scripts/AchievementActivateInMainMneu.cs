using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementActivateInMainMneu : MonoBehaviour
{
	[SerializeField] List<GameObject> AchievementFilter;

	private void OnEnable()
	{
		for (int i = 0; i < AchievementManager.Instance.achievements.Count; i++)
		{
			if (AchievementManager.Instance.achievements[i].isUnlocked)
			{
				AchievementFilter[i].SetActive(false);
			}
			else { AchievementFilter[i].SetActive(true); }
		}
	}
	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
