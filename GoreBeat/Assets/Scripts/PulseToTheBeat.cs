using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseToTheBeat : MonoBehaviour
{
	[Header("Normal Pulse")]
	[SerializeField] float pulseSize = 1.15f;
	[SerializeField] float returnSpeed = 5f;
	private Vector3 startSize;

	[Header("Custom Pulse")]
	[SerializeField] string customAnimation = "None";
	[SerializeField] Animator anim;

	private void Awake()
	{
		if (customAnimation.Equals("None")) { return; }
		if(anim == null) { anim = GetComponent<Animator>(); }
	}
	void Start()
    {
		startSize = transform.localScale;
    }
    void Update()
    {
		transform.localScale = Vector3.Lerp(transform.localScale, startSize, Time.deltaTime * returnSpeed);
    }
	public void Pulse()
	{
		transform.localScale = startSize * pulseSize;
	}
	public void CustomPulse()
	{
		anim.SetTrigger(customAnimation);
	}
}
