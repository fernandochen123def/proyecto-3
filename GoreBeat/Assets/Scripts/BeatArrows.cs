using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using System;
using UnityEngine.SceneManagement;

public class BeatArrows : MonoBehaviour
{
	[Header("Arrow Images")]
	[SerializeField] private Image currentLeftArrow, nextLeftArrow;
	[SerializeField] private Image currentRightArrow, nextRightArrow;
	[SerializeField] private Image holeLeftArrowImage, holeRightArrowImage;

	[Header("Feedback Arrow Images")]
	[SerializeField] private Image feedbackLeftArrowPerfect, feedbackRightArrowPerfect;
	[SerializeField] private Image feedbackLeftArrowGood, feedbackRightArrowGood;
	[SerializeField] private Image feedbackLeftArrowDecent, feedbackRightArrowDecent;
	[SerializeField] private Image feedbackLeftArrowMissed, feedbackRightArrowMissed;

	[Header("UI Elements")]
	[SerializeField] private TextMeshProUGUI feedbackText;
	[SerializeField] private TextMeshProUGUI timerText;

	[Header("Beat Settings")]
	public float bpm = 140.0f;
	[SerializeField] float startDelay = 0;
	public float calibrationOffset = 0;
	[SerializeField] AudioSource audioSource;
	[SerializeField] AudioSource winSource;

	private Coroutine moveCurrentLeftCoroutine;
	private Coroutine moveCurrentRightCoroutine;
	private Coroutine moveNextLeftCoroutine;
	private Coroutine moveNextRightCoroutine;
	private Coroutine fadeNextLeftCoroutine;
	private Coroutine fadeNextRightCoroutine;
	public bool actioned;
	float timeSurvivedInSeconds = 0f;

	[HideInInspector]
	public float beatInterval;
	[SerializeField] private Intervals[] intervals;
	Player player;
	[System.Serializable]
	public class Intervals
	{
		[SerializeField] private float steps;
		[SerializeField] private UnityEvent trigger;
		private int lastInterval;

		public float GetIntervalLength(float bpm) { return 60f / (bpm * steps); }

		public void CheckForNewInterval (float interval)
		{
			if(Mathf.FloorToInt(interval) != lastInterval)
			{
				lastInterval = Mathf.FloorToInt(interval);
				trigger.Invoke();
			}
		}
	}

	void Start()
	{
		calibrationOffset = PlayerPrefs.GetFloat("CalibrationOffset");

		beatInterval = 60.0f / bpm;

		InitializeArrows();

		audioSource.PlayDelayed(startDelay);
		player = FindAnyObjectByType<Player>();

	}

	private void Update()
	{
		beatInterval = 60.0f / bpm;

		int offsetInSamples = (int)(calibrationOffset / 1000.0f * audioSource.clip.frequency);

		// Adjust the timeSamples by this offset
		// Add the offset because a positive calibrationOffset should simulate a delay in hearing the audio
		foreach (Intervals interval in intervals)
		{
			float sampledTime = ((audioSource.timeSamples + offsetInSamples) / (audioSource.clip.frequency * interval.GetIntervalLength(bpm)));
			interval.CheckForNewInterval(sampledTime);
		}

		if (!GetComponent<PauseMenu>().isPaused)
		{
			UpdateTimer();
		}
	}

	private void InitializeArrows()
	{
		// Current arrows
		currentLeftArrow.rectTransform.anchoredPosition = new Vector2(-Screen.width / 8f, 0);
		currentRightArrow.rectTransform.anchoredPosition = new Vector2(Screen.width / 8f, 0);
		currentLeftArrow.gameObject.SetActive(true);
		currentRightArrow.gameObject.SetActive(true);

		// Preview arrows
		nextLeftArrow.rectTransform.anchoredPosition = new Vector2(-Screen.width / 4f, 0);
		nextRightArrow.rectTransform.anchoredPosition = new Vector2(Screen.width / 4f, 0);
		nextLeftArrow.color = new Color(1, 1, 1, 0.0f);  // Start at 0 opacity for preview
		nextRightArrow.color = new Color(1, 1, 1, 0.0f); // Start at 0 opacity for preview
		nextLeftArrow.gameObject.SetActive(true);
		nextRightArrow.gameObject.SetActive(true);
	}

	public void OnBeat()
	{
		actioned = false;

		// Stop current arrow movement coroutines in order to resync
		if (moveCurrentLeftCoroutine != null)
			StopCoroutine(moveCurrentLeftCoroutine);
		if (moveCurrentRightCoroutine != null)
			StopCoroutine(moveCurrentRightCoroutine);
		if (moveNextLeftCoroutine != null)
			StopCoroutine(moveNextLeftCoroutine);
		if (moveNextRightCoroutine != null)
			StopCoroutine(moveNextRightCoroutine);
		if (fadeNextLeftCoroutine != null)
			StopCoroutine(fadeNextLeftCoroutine);
		if (fadeNextRightCoroutine != null)
			StopCoroutine(fadeNextRightCoroutine);

		InitializeArrows(); // Re-initialize arrows to reset positions and states

		// Start new arrow coroutines with references stored to be able to stop them later
		moveCurrentLeftCoroutine = StartCoroutine(MoveArrowsToCenter(currentLeftArrow, holeLeftArrowImage.rectTransform.anchoredPosition + new Vector2(20, 0), beatInterval));
		moveCurrentRightCoroutine = StartCoroutine(MoveArrowsToCenter(currentRightArrow, holeRightArrowImage.rectTransform.anchoredPosition + new Vector2(-20, 0), beatInterval));
		moveNextLeftCoroutine = StartCoroutine(MoveArrowsToCenter(nextLeftArrow, currentLeftArrow.rectTransform.anchoredPosition, beatInterval));
		moveNextRightCoroutine = StartCoroutine(MoveArrowsToCenter(nextRightArrow, currentRightArrow.rectTransform.anchoredPosition, beatInterval));
		fadeNextLeftCoroutine = StartCoroutine(FadeInPreviewArrow(nextLeftArrow, currentLeftArrow.rectTransform.anchoredPosition, beatInterval));
		fadeNextRightCoroutine = StartCoroutine(FadeInPreviewArrow(nextRightArrow, currentRightArrow.rectTransform.anchoredPosition, beatInterval));
	}

	IEnumerator MoveArrowsToCenter(Image arrow, Vector2 targetPosition, float duration)
	{
		Vector2 startPosition = arrow.rectTransform.anchoredPosition;
		float elapsedTime = 0;

		while (elapsedTime < duration)
		{
			elapsedTime += Time.deltaTime;
			arrow.rectTransform.anchoredPosition = Vector2.Lerp(startPosition, targetPosition, elapsedTime / duration);
			yield return null;
		}
	}
	IEnumerator FadeInPreviewArrow(Image arrow, Vector2 targetPosition, float duration)
	{
		float startOpacity = 0.0f;
		float endOpacity = 1.0f;
		float startPositionX = arrow.rectTransform.anchoredPosition.x;
		float targetPositionX = targetPosition.x;
		float distanceToTravel = Mathf.Abs(targetPositionX - startPositionX);
		float fadeInStartX = startPositionX + (distanceToTravel * 0.0f * Mathf.Sign(targetPositionX - startPositionX)); // Start fading when 0% towards target

		// Adjust while condition to check based on direction of movement
		Func<bool> shouldContinue = startPositionX < targetPositionX ?
			(() => arrow.rectTransform.anchoredPosition.x < targetPositionX) :
			(() => arrow.rectTransform.anchoredPosition.x > targetPositionX);

		while (shouldContinue())
		{
			float currentX = arrow.rectTransform.anchoredPosition.x;
			float progress = Mathf.InverseLerp(fadeInStartX, targetPositionX, currentX);

			// Update opacity
			float newOpacity = Mathf.Lerp(startOpacity, endOpacity, progress);
			arrow.color = new Color(arrow.color.r, arrow.color.g, arrow.color.b, newOpacity);

			yield return null;
		}

		// Ensure it's at full opacity when it reaches or exceeds the target position
		arrow.color = new Color(arrow.color.r, arrow.color.g, arrow.color.b, endOpacity);
	}

	public TimingFeedback OnPlayerAction()
	{
		actioned = true;
		TimingFeedback feedback = DetermineFeedback();
		feedbackText.text = feedback.ToString();
		StartCoroutine(ClearFeedbackText());
		Image leftFeedbackArrow = GetFeedbackArrow(feedback, true);
		Image rightFeedbackArrow = GetFeedbackArrow(feedback, false);
		currentLeftArrow.gameObject.SetActive(false);
		currentRightArrow.gameObject.SetActive(false);
		StartCoroutine(ShowAndFadeFeedbackArrow(leftFeedbackArrow, currentLeftArrow.rectTransform.anchoredPosition));
		StartCoroutine(ShowAndFadeFeedbackArrow(rightFeedbackArrow, currentRightArrow.rectTransform.anchoredPosition));
		return feedback;
	}

	IEnumerator ClearFeedbackText()
	{
		yield return new WaitForSeconds(beatInterval/2);
		feedbackText.text = "";
	}

	private TimingFeedback DetermineFeedback()
	{
		float distance = Vector2.Distance(currentLeftArrow.rectTransform.anchoredPosition, holeLeftArrowImage.rectTransform.anchoredPosition);
		if (distance < 10) return TimingFeedback.Perfect;
		else if (distance < 25) return TimingFeedback.Good;
		else if (distance < 50) return TimingFeedback.Decent;
		else return TimingFeedback.Missed;
	}

	private Image GetFeedbackArrow(TimingFeedback feedback, bool isLeft)
	{
		switch (feedback)
		{
			case TimingFeedback.Perfect: return isLeft ? feedbackLeftArrowPerfect : feedbackRightArrowPerfect;
			case TimingFeedback.Good: return isLeft ? feedbackLeftArrowGood : feedbackRightArrowGood;
			case TimingFeedback.Decent: return isLeft ? feedbackLeftArrowDecent : feedbackRightArrowDecent;
			case TimingFeedback.Missed: return isLeft ? feedbackLeftArrowMissed : feedbackRightArrowMissed;
			default: return null;
		}
	}

	IEnumerator ShowAndFadeFeedbackArrow(Image feedbackArrow, Vector2 position)
	{
		feedbackArrow.rectTransform.anchoredPosition = position;
		feedbackArrow.gameObject.SetActive(true);
		float fadeDuration = beatInterval/2;
		Color startColor = Color.white;
		for (float t = 0; t < fadeDuration; t += Time.deltaTime)
		{
			feedbackArrow.color = Color.Lerp(startColor, new Color(startColor.r, startColor.g, startColor.b, 0), t / fadeDuration);
			yield return null;
		}
		feedbackArrow.gameObject.SetActive(false);
	}

	private void UpdateTimer()
	{
		float remainingTime = audioSource.clip.length - audioSource.time;

		if (!audioSource.isPlaying && remainingTime <= 0)
		{
			StartCoroutine(AnimatorWin());
			return;
		}

		TimeSpan timeSpan = TimeSpan.FromSeconds(remainingTime);
		timerText.text = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
	}

	private void TrackCompleted()
	{
		audioSource.Stop();
		Debug.Log("Track Completed");

		timeSurvivedInSeconds = audioSource.clip.length;

		if(FindAnyObjectByType<Player>().health == FindAnyObjectByType<Player>().maxHealth) { AchievementManager.Instance.UnlockAchievement("untouchable"); }

		// Unlock next level if needed
		UnlockNextLevel();

		// Show score screen
		GameOver();
	}

	private void UnlockNextLevel()
	{
		string currentScene = SceneManager.GetActiveScene().name;

		if (currentScene == SceneLoaderComplex.Instance.TutorialScene)
		{
			if (!SaveManager.Singleton.IsFirstLevelUnlocked())
			{
				SaveManager.Singleton.UnlockLevel(1);
			}
		}
		else if (currentScene == SceneLoaderComplex.Instance.FirstLevel)
		{
			if (!SaveManager.Singleton.IsSecondLevelUnlocked())
			{
				SaveManager.Singleton.UnlockLevel(2);
				SaveManager.Singleton.UnlockSecondFist();
			}
		}
		else if (currentScene == SceneLoaderComplex.Instance.SecondLevel)
		{
			if (!SaveManager.Singleton.IsThirdLevelUnlocked())
			{
				SaveManager.Singleton.UnlockLevel(3);
				SaveManager.Singleton.UnlockThirdFist();
			}
		}
	}

	public void GameOver()
	{
		Debug.Log("Game Over");

		if(timeSurvivedInSeconds == 0)
		{
			timeSurvivedInSeconds = audioSource.time;
		}

		if(timeSurvivedInSeconds < 60) { AchievementManager.Instance.UnlockAchievement("speedrun"); }

		// Update high score based on current level
		UpdateHighScore(timeSurvivedInSeconds);

		// Show score screen
		SceneLoaderComplex.Instance.LoadScenes(new string[] { SceneLoaderComplex.Instance.ScoreBoard }, true);
	}

	private void UpdateHighScore(float timeSurvivedInSeconds)
	{
		string currentScene = SceneManager.GetActiveScene().name;
		int currentScore = Player.Instance.Score;
		int currentStreak = Player.Instance.HighestStreak;
		TimeSpan timeSpan = TimeSpan.FromSeconds(timeSurvivedInSeconds);
		string timeSurvived = string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
		string grade = Player.Instance.Grade;

		// Update ScoreData (Temporary score of last level played to show in scoreboard)
		ScoreData.Score = currentScore;
		ScoreData.TimeSurvived = timeSurvived;
		ScoreData.Grade = grade;
		ScoreData.HighestStreak = currentStreak;

		if (currentScene == SceneLoaderComplex.Instance.FirstLevel)
		{
			SaveManager.Singleton.UpdateHighScore(1, currentScore, currentStreak, timeSurvived, grade);
		}
		else if (currentScene == SceneLoaderComplex.Instance.SecondLevel)
		{
			SaveManager.Singleton.UpdateHighScore(2, currentScore, currentStreak, timeSurvived, grade);
		}
		else if (currentScene == SceneLoaderComplex.Instance.ThirdLevel)
		{
			if(timeSurvivedInSeconds == audioSource.clip.length) { AchievementManager.Instance.UnlockAchievement("level3"); }
			SaveManager.Singleton.UpdateHighScore(3, currentScore, currentStreak, timeSurvived, grade);
		}

		if (grade.Equals("S+"))
		{
			AchievementManager.Instance.UnlockAchievement("perfect");
			SaveManager.Singleton.AllPerfectCheck();
		}
	}
	IEnumerator AnimatorWin()
	{
		player.ActiveWinAnimation();
		winSource.Play();
		yield return new WaitForSeconds(1f);
		TrackCompleted();
		player.DesactiveWinAnimation();

	}
}