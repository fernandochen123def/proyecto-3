using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchVFX : MonoBehaviour
{
	[Header("SFX")]
	[SerializeField] public AudioSource punchSound;
	[SerializeField] public AudioSource reloadSound;

	[Header("VFX")]
	[SerializeField] public Transform vfxMuzzleSpawner;
	[SerializeField] public Transform vfxMuzzle;
	public void MuzzleExplosion()
	{
		Instantiate(vfxMuzzle, vfxMuzzleSpawner.transform.position, Quaternion.identity);
	}
	  
    public void PlayPunchSound()
	{
		punchSound.Play();
	}public void PlayReloadSound()
	{
		reloadSound.Play();
	}
}
