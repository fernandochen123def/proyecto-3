using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class CalibrationArrows : MonoBehaviour
{
	[Header("Arrow Images")]
	[SerializeField] private Image currentLeftArrow, nextLeftArrow;
	[SerializeField] private Image currentRightArrow, nextRightArrow;
	[SerializeField] private Image holeLeftArrowImage, holeRightArrowImage;

	[Header("UI Elements")]
	[SerializeField] private TextMeshProUGUI calibrationResultText;
	[SerializeField] private Slider delaySlider;
	[SerializeField] private Button continueButton;

	[Header("Beat Settings")]
	public float bpm = 140.0f;
	[SerializeField] private float startDelay = 0;
	[SerializeField] private AudioSource audioSource;

	private Coroutine moveCurrentLeftCoroutine;
	private Coroutine moveCurrentRightCoroutine;
	private Coroutine moveNextLeftCoroutine;
	private Coroutine moveNextRightCoroutine;
	private Coroutine fadeNextLeftCoroutine;
	private Coroutine fadeNextRightCoroutine;
	public bool actioned;

	private float beatInterval;
	private float calibrationOffset = 0;

	[SerializeField] private Interval[] intervals;

	[System.Serializable]
	public class Interval
	{
		public float steps;
		public UnityEngine.Events.UnityEvent trigger;
		private int lastInterval;

		public float GetIntervalLength(float bpm) { return 60f / (bpm * steps); }

		public void CheckForNewInterval(float interval)
		{
			if (Mathf.FloorToInt(interval) != lastInterval)
			{
				lastInterval = Mathf.FloorToInt(interval);
				trigger.Invoke();
			}
		}
	}

	void Start()
	{
		beatInterval = 60.0f / bpm;
		InitializeArrows();
		audioSource.PlayDelayed(startDelay);

		delaySlider.minValue = 0;
		delaySlider.maxValue = beatInterval * 1000f; // max value in milliseconds
		delaySlider.value = PlayerPrefs.GetFloat("CalibrationOffset");
		UpdateCalibrationOffset(PlayerPrefs.GetFloat("CalibrationOffset"));
		delaySlider.onValueChanged.AddListener(UpdateCalibrationOffset);
		continueButton.onClick.AddListener(SaveCalibrationOffset);
	}

	private void Update()
	{
		beatInterval = 60.0f / bpm;
		int offsetInSamples = (int)(calibrationOffset / 1000.0f * audioSource.clip.frequency);

		foreach (var interval in intervals)
		{
			float sampledTime = ((audioSource.timeSamples + offsetInSamples) / (audioSource.clip.frequency * interval.GetIntervalLength(bpm)));
			interval.CheckForNewInterval(sampledTime);
		}
	}

	private void InitializeArrows()
	{
		// Current arrows
		currentLeftArrow.rectTransform.anchoredPosition = new Vector2(-Screen.width / 8f, 0);
		currentRightArrow.rectTransform.anchoredPosition = new Vector2(Screen.width / 8f, 0);
		currentLeftArrow.gameObject.SetActive(true);
		currentRightArrow.gameObject.SetActive(true);

		// Preview arrows
		nextLeftArrow.rectTransform.anchoredPosition = new Vector2(-Screen.width / 4f, 0);
		nextRightArrow.rectTransform.anchoredPosition = new Vector2(Screen.width / 4f, 0);
		nextLeftArrow.color = new Color(nextLeftArrow.color.r, nextLeftArrow.color.g, nextLeftArrow.color.b, 0.0f);  // Start at 0 opacity for preview
		nextRightArrow.color = new Color(nextRightArrow.color.r, nextRightArrow.color.g, nextRightArrow.color.b, 0.0f); // Start at 0 opacity for preview
		nextLeftArrow.gameObject.SetActive(true);
		nextRightArrow.gameObject.SetActive(true);
	}

	public void OnBeat()
	{
		actioned = false;

		// Stop current arrow movement coroutines in order to resync
		if (moveCurrentLeftCoroutine != null)
			StopCoroutine(moveCurrentLeftCoroutine);
		if (moveCurrentRightCoroutine != null)
			StopCoroutine(moveCurrentRightCoroutine);
		if (moveNextLeftCoroutine != null)
			StopCoroutine(moveNextLeftCoroutine);
		if (moveNextRightCoroutine != null)
			StopCoroutine(moveNextRightCoroutine);
		if (fadeNextLeftCoroutine != null)
			StopCoroutine(fadeNextLeftCoroutine);
		if (fadeNextRightCoroutine != null)
			StopCoroutine(fadeNextRightCoroutine);

		InitializeArrows(); // Re-initialize arrows to reset positions and states

		// Start new arrow coroutines with references stored to be able to stop them later
		moveCurrentLeftCoroutine = StartCoroutine(MoveArrowsToCenter(currentLeftArrow, holeLeftArrowImage.rectTransform.anchoredPosition + new Vector2(20, 0), beatInterval));
		moveCurrentRightCoroutine = StartCoroutine(MoveArrowsToCenter(currentRightArrow, holeRightArrowImage.rectTransform.anchoredPosition + new Vector2(-20, 0), beatInterval));
		moveNextLeftCoroutine = StartCoroutine(MoveArrowsToCenter(nextLeftArrow, currentLeftArrow.rectTransform.anchoredPosition, beatInterval));
		moveNextRightCoroutine = StartCoroutine(MoveArrowsToCenter(nextRightArrow, currentRightArrow.rectTransform.anchoredPosition, beatInterval));
		fadeNextLeftCoroutine = StartCoroutine(FadeInPreviewArrow(nextLeftArrow, currentLeftArrow.rectTransform.anchoredPosition, beatInterval));
		fadeNextRightCoroutine = StartCoroutine(FadeInPreviewArrow(nextRightArrow, currentRightArrow.rectTransform.anchoredPosition, beatInterval));
	}

	IEnumerator MoveArrowsToCenter(Image arrow, Vector2 targetPosition, float duration)
	{
		Vector2 startPosition = arrow.rectTransform.anchoredPosition;
		float elapsedTime = 0;

		while (elapsedTime < duration)
		{
			elapsedTime += Time.deltaTime;
			arrow.rectTransform.anchoredPosition = Vector2.Lerp(startPosition, targetPosition, elapsedTime / duration);
			yield return null;
		}
	}

	IEnumerator FadeInPreviewArrow(Image arrow, Vector2 targetPosition, float duration)
	{
		float startOpacity = 0.0f;
		float endOpacity = 1.0f;
		float startPositionX = arrow.rectTransform.anchoredPosition.x;
		float targetPositionX = targetPosition.x;
		float distanceToTravel = Mathf.Abs(targetPositionX - startPositionX);
		float fadeInStartX = startPositionX + (distanceToTravel * 0.0f * Mathf.Sign(targetPositionX - startPositionX)); // Start fading when 0% towards target

		// Adjust while condition to check based on direction of movement
		Func<bool> shouldContinue = startPositionX < targetPositionX ?
			(() => arrow.rectTransform.anchoredPosition.x < targetPositionX) :
			(() => arrow.rectTransform.anchoredPosition.x > targetPositionX);

		while (shouldContinue())
		{
			float currentX = arrow.rectTransform.anchoredPosition.x;
			float progress = Mathf.InverseLerp(fadeInStartX, targetPositionX, currentX);

			// Update opacity
			float newOpacity = Mathf.Lerp(startOpacity, endOpacity, progress);
			arrow.color = new Color(arrow.color.r, arrow.color.g, arrow.color.b, newOpacity);

			yield return null;
		}

		// Ensure it's at full opacity when it reaches or exceeds the target position
		arrow.color = new Color(arrow.color.r, arrow.color.g, arrow.color.b, endOpacity);
	}

	private void UpdateCalibrationOffset(float value)
	{
		calibrationOffset = value;
		calibrationResultText.text = "Calibration Offset: " + calibrationOffset.ToString("F2") + "ms";

		// Apply calibration immediately
		int offsetInSamples = (int)(calibrationOffset / 1000.0f * audioSource.clip.frequency);
		audioSource.timeSamples = (audioSource.timeSamples + offsetInSamples) % audioSource.clip.samples;
	}

	private void SaveCalibrationOffset()
	{
		PlayerPrefs.SetFloat("CalibrationOffset", calibrationOffset);
		PlayerPrefs.Save();
		//continueButton.gameObject.SetActive(false);
	}
}
