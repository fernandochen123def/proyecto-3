using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodCrystalManager : MonoBehaviour
{
	public static BloodCrystalManager Instance;

	[SerializeField] private List<GameObject> crystals; 
	[SerializeField] private float respawnTime = 25f;
	[SerializeField] private int maxActiveCrystals = 2;

	private int currentActiveCount;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
		}

		foreach (var crystal in crystals)
		{
			crystal.SetActive(false);
		}

		ActivateInitialCrystals();
	}

	private void ActivateInitialCrystals()
	{
		currentActiveCount = 0;
		while (currentActiveCount < maxActiveCrystals)
		{
			ActivateRandomCrystal();
		}
	}

	public void CrystalPickedUp(GameObject pickedUpCrystal)
	{
		pickedUpCrystal.SetActive(false);
		currentActiveCount--;
		StartCoroutine(RespawnCrystal());
	}

	private IEnumerator RespawnCrystal()
	{
		yield return new WaitForSeconds(respawnTime);
		if (currentActiveCount < maxActiveCrystals)
		{
			ActivateRandomCrystal();
		}
	}

	private void ActivateRandomCrystal()
	{
		List<GameObject> inactiveCrystals = new List<GameObject>();
		foreach (var crystal in crystals)
		{
			if (!crystal.activeInHierarchy)
			{
				inactiveCrystals.Add(crystal);
			}
		}

		if (inactiveCrystals.Count > 0)
		{
			int randomIndex = Random.Range(0, inactiveCrystals.Count);
			inactiveCrystals[randomIndex].SetActive(true);
			currentActiveCount++;
		}
	}
}
