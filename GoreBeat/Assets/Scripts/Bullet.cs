using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float damage = 50f;
	[SerializeField] private GameObject vfxBulletImpact;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Enemy"))
		{
			Enemy enemy = other.GetComponent<Enemy>();
			if (enemy != null)
			{
				enemy.TakeDamage(damage);
			}
		}
		Instantiate(vfxBulletImpact, transform.position, Quaternion.identity);
		Destroy(gameObject);
	}
}
