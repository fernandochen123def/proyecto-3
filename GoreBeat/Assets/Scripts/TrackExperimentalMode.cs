using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class TrackExperimentalMode : MonoBehaviour
{
	[SerializeField] AudioSource trackLevel;
	[SerializeField] AudioClip trackIA;
	[SerializeField] AudioClip trackRocio;
	private bool isExperimentalBoolActive;
	// Start is called before the first frame update
	private void Awake()
	{
		isExperimentalBoolActive = DataStore.ExperimentalMode;
		
	}
	void Start()
    {
		if (isExperimentalBoolActive)
		{
			trackLevel.clip = trackRocio;
			trackLevel.Play();
		}
		else
		{
			trackLevel.clip = trackIA;
			trackLevel.Play();
		}
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
