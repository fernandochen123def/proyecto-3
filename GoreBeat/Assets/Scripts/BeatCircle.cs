using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public enum TimingFeedback
{
	Perfect,
	Good,
	Decent,
	Missed
}

public class BeatCircle : MonoBehaviour
{
	[SerializeField] private Image innerCircleImage;
	[SerializeField] private float bpm = 120.0f;
	[SerializeField] private TextMeshProUGUI feedbackText;

	private float beatInterval;
	private float nextBeatTime = 0f;
	private float scaleDuration;
	private Vector3 minScale = new Vector3(0.5f, 0.5f, 1f);
	private Vector3 maxScale = new Vector3(2f, 2f, 1f);

	private const float TargetScale = 1f;
	private const float ScaleRangeForPerfect = 0.05f;

	void Start()
	{
		beatInterval = 60f / bpm;
		nextBeatTime = Time.time;
		scaleDuration = CalculateScaleDuration(bpm);
		StartCoroutine(ScaleAndFadeInnerCircleLoop());
	}

	IEnumerator ScaleAndFadeInnerCircleLoop()
	{
		while (true)
		{
			yield return StartCoroutine(ScaleAndFadeInnerCircle());
			nextBeatTime += beatInterval;
		}
	}

	IEnumerator ScaleAndFadeInnerCircle()
	{
		float currentTime = 0f;
		innerCircleImage.transform.localScale = minScale;
		Color startColor = innerCircleImage.color;
		Color fullOpacityColor = new Color(startColor.r, startColor.g, startColor.b, 1);

		while (currentTime < scaleDuration / 2f)
		{
			currentTime += Time.deltaTime;
			float t = currentTime / (scaleDuration / 2f);
			innerCircleImage.transform.localScale = Vector3.Lerp(maxScale, minScale, t);
			yield return null;
		}

		currentTime = 0f;
		while (currentTime < scaleDuration / 2f)
		{
			currentTime += Time.deltaTime;
			float t = currentTime / (scaleDuration / 2f);
			innerCircleImage.color = new Color(startColor.r, startColor.g, startColor.b, Mathf.Lerp(1, 0, t));
			yield return null;
		}

		innerCircleImage.transform.localScale = maxScale;
		innerCircleImage.color = fullOpacityColor;
	}

	public TimingFeedback GetCurrentTimingFeedback()
	{
		float currentScale = innerCircleImage.transform.localScale.x;
		TimingFeedback feedback = DetermineFeedback(currentScale);
		feedbackText.text = feedback.ToString();
		StartCoroutine(ClearFeedbackText());
		return feedback;
	}
	IEnumerator ClearFeedbackText()
	{
		yield return new WaitForSeconds(0.5f);
		feedbackText.text = "";
	}

	private TimingFeedback DetermineFeedback(float currentScale)
	{
		float distanceFromTarget = Mathf.Abs(currentScale - TargetScale);
		if (distanceFromTarget <= ScaleRangeForPerfect) return TimingFeedback.Perfect;
		else if (distanceFromTarget <= ScaleRangeForPerfect * 2) return TimingFeedback.Good;
		else if (distanceFromTarget <= ScaleRangeForPerfect * 3) return TimingFeedback.Decent;
		else return TimingFeedback.Missed;
	}

	private float CalculateScaleDuration(float bpm)
	{
		return 2f * (60f / bpm);
	}
}
