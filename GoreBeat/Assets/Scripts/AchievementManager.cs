using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementManager : MonoBehaviour
{
	public static AchievementManager Instance { get; private set; }

	[SerializeField] public List<Achievement> achievements;
	AchievementAnimation achievementAnimation;
	bool unlocking = false;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
		}
	}

	private void Start()
	{
		SaveManager.Singleton.LoadAchievements();
	}

	public void IncrementAchievement(string achievementID, int amount)
	{
		Achievement achievement = achievements.Find(a => a.achievementID == achievementID);
		if (achievement != null)
		{
			achievement.IncrementProgress(amount);
		}
	}

	public void UnlockAchievement(string achievementID)
	{
		Achievement achievement = achievements.Find(a => a.achievementID == achievementID);
		if (achievement != null && !achievement.isUnlocked)
		{
			Debug.Log($"Respuesta >{achievementID}< longitud:{achievementID.Length}");
			achievement.Unlock();
			SaveManager.Singleton.SaveAchievement(achievement.achievementID, true);
			achievementAnimation = FindAnyObjectByType<AchievementAnimation>();
			StartCoroutine(ActiveAnimationAchivement(achievementID));
			CheckCompletionist();
		}
	}
	private void CheckCompletionist()
	{
		if (achievements.FindAll(a => a.achievementID != "completionist").TrueForAll(a => a.isUnlocked))
		{
			UnlockAchievement("completionist");
		}
	}
	public void UnlockOrResetAllAchievements(bool unlock)
	{
		foreach(Achievement achievement in achievements) { achievement.isUnlocked = unlock; }
	}

	public Achievement GetAchievement(string achievementID)
	{
		return achievements.Find(a => a.achievementID == achievementID);
	}
	
	IEnumerator ActiveAnimationAchivement(string achievementID)
	{
		if (unlocking) { yield return new WaitForSeconds(4f); }
		unlocking = true;
		achievementAnimation.activeAnimation();
		Achievement achievement = GetAchievement(achievementID);
		achievementAnimation.icon.sprite = achievement.icon;
		achievementAnimation.title.text = achievement.title;
		achievementAnimation.description.text = achievement.description;
		yield return new WaitForSeconds(2f);
		achievementAnimation.desactiveAnimation();
		unlocking = false;
	}
}
