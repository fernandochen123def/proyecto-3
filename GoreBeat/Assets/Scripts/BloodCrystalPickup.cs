using UnityEngine;

public class BloodCrystalPickup : MonoBehaviour
{
	[SerializeField] private float heatAmount = 4f;

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			Player player = other.GetComponent<Player>();
			if (player != null)
			{
				player.IncreaseHeat(heatAmount);
				BloodCrystalManager.Instance.CrystalPickedUp(gameObject);
			}
		}
	}
}
