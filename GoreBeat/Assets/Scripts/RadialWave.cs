using System.Collections;
using UnityEngine;

public class RadialWave : MonoBehaviour
{
	[Header("Wave Settings")]
	[SerializeField] private GameObject waveVFXPrefab;
	[SerializeField] private float maxScale = 10f;
	[SerializeField] private float maxDamageRadius = 60f;
	[SerializeField] private float scaleDuration = 2f;
	[SerializeField] private LayerMask damageLayerMask;
	[SerializeField] private LayerMask obstacleLayerMask;
	[SerializeField] private float damageAmount = 25f;

	private bool damageDealt = false;

	public void SpawnRadialWave()
	{
		GameObject waveVFX = Instantiate(waveVFXPrefab, transform.position, Quaternion.identity);
		waveVFX.transform.localScale = Vector3.zero;
		StartCoroutine(ScaleWave(waveVFX));
	}

	private IEnumerator ScaleWave(GameObject waveVFX)
	{
		float elapsedTime = 0f;
		Vector3 initialScale = Vector3.zero;
		Vector3 targetScale = new Vector3(maxScale, maxScale, maxScale);

		while (elapsedTime < scaleDuration)
		{
			elapsedTime += Time.deltaTime;
			float t = elapsedTime / scaleDuration;

			waveVFX.transform.localScale = Vector3.Lerp(initialScale, targetScale, t);
			float currentRadius = Mathf.Lerp(0, maxDamageRadius, t);

			ApplyDamage(waveVFX.transform.position, currentRadius);

			yield return null;
		}

		waveVFX.transform.localScale = targetScale;
		ApplyDamage(waveVFX.transform.position, maxDamageRadius);
		Destroy(waveVFX);

		damageDealt = false;
	}

	private void ApplyDamage(Vector3 center, float radius)
	{
		if (damageDealt) return;

		Collider[] hitColliders = Physics.OverlapSphere(center, radius, damageLayerMask);

		foreach (Collider hitCollider in hitColliders)
		{
			if (IsDamageBlocked(center, hitCollider.transform.position))
			{
				continue;
			}

			Player player = hitCollider.GetComponent<Player>();
			if (player != null)
			{
				player.TakeDamage(damageAmount);
				damageDealt = true;
				break;  
			}
		}
	}

	private bool IsDamageBlocked(Vector3 explosionCenter, Vector3 targetPosition)
	{
		Vector3 direction = targetPosition - explosionCenter;
		float distance = Vector3.Distance(explosionCenter, targetPosition);

		RaycastHit hit;
		if (Physics.Raycast(explosionCenter, direction, out hit, distance, obstacleLayerMask))
		{
			return true;
		}

		return false;
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, maxDamageRadius);
	}
}
