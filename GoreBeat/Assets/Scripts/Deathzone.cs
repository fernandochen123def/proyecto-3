using StarterAssets;
using System.Collections;
using UnityEngine;

public class Deathzone : MonoBehaviour
{
	Player player;
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			FirstPersonController controller = other.GetComponent<FirstPersonController>();
			player = other.GetComponent<Player>();

			if (controller != null)
			{
				controller.enabled = false;
				Transform checkpoint = CheckpointManager.Instance.GetCheckpoint();
				if (checkpoint != null)
				{
					other.transform.position = checkpoint.position;
					Debug.Log("Player respawned at: " + checkpoint.name);
				}
				else
				{
					Debug.Log("No checkpoint set, respawning at default position or handling death differently.");
				}
				StartCoroutine(ReenableController(controller));
			}

			if (other.gameObject.scene.name.Equals(SceneLoaderComplex.Instance.TutorialScene)) { AchievementManager.Instance.UnlockAchievement("pathetic"); }
		}
	}

	private IEnumerator ReenableController(FirstPersonController controller)
	{
		if (player != null)
		{
			player.TakeDamage(25f);
		}
			yield return new WaitForSeconds(0.1f);
		controller.enabled = true;
	}
}