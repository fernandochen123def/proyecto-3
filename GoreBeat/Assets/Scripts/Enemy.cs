using PampelGames.GoreSimulator;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.AI;
using UnityEngine.VFX;

public class Enemy : MonoBehaviour
{
	[Header("Enemy Settings")]
	[SerializeField] private float health = 100f;
	[SerializeField] private Renderer enemyRenderer;
	public enum EnemyType { Basic, Explosive, Distance }

	[Header("Flash Damage Settings")]
	[SerializeField] private Material flashDamageMaterial;
	private Material[] originalMaterials;

	[Header("General Settings")]
	public EnemyType type;
	[SerializeField] private Transform target;
	[SerializeField] private float chaseRange = 10f;
	[SerializeField] private float attackRange = 2f;
	[SerializeField] private float moveSpeed = 3.5f;
	[SerializeField] private float stopDistance = 1.5f;

	[Header("Attack Settings")]
	public float attackDamage = 5f;
	[SerializeField] private float attackInterval = 1.5f;

	[Header("Explosive Settings")]
	[SerializeField] private float explosionDelay = 1.5f;
	[SerializeField] private float explosionDamage = 50f;
	[SerializeField] private float explosionRange = 5f;
	[SerializeField] private float explosiveStopDistance = 3f;
	[SerializeField] private float scaleFactor = 0.5f;

	[Header("Distance Enemy Settings")]
	[SerializeField] private GameObject projectilePrefab;
	[SerializeField] private Transform shootingOrigin;
	[SerializeField] private float shootingRange = 15f;
	[SerializeField] private float minimumSafeDistance = 10f;
	[SerializeField] private float shootingInterval = 2f;
	private float shootingTimer;

	private NavMeshAgent agent;
	private float attackTimer;
	private float explosionTimer = 0;
	Vector3 defaultScale;
	private bool isExploding;

	[Header("Animator")]
	[SerializeField] private string isDeadAnim = "IsDead";
	[SerializeField] private string Hit = "Hit";
	[SerializeField] private string Run = "Running";
	[SerializeField] private string AttackAnim = "Attack";

	[Header("VFX")]
	[SerializeField] private float dissolveRate = 0.0125f;
	[SerializeField] private VisualEffect VFXGraph;
	[SerializeField] private GameObject bloodExplosion;
	[SerializeField] private Transform bloodSpawnExplosion;
	[SerializeField] private float refreshRate = 0.025f;
	[SerializeField] private float dieDelay = 0.2f;
	[SerializeField] private float counter;

	[Header("SFX")]
	[SerializeField] private AudioSource sfxEnemy;
	[SerializeField] private AudioSource deathSound;
	[SerializeField] private AudioSource enemyShotSound;


	private Material[] skinnedRendererMaterial;
	private Animator animator;
	private CapsuleCollider capsuleCollider;
	private bool isDead = false;

	Player player;

	private void Awake()
	{
		capsuleCollider = GetComponent<CapsuleCollider>();
		if(type != EnemyType.Explosive) { animator = GetComponent<Animator>(); }
		agent = GetComponent<NavMeshAgent>();
		agent.stoppingDistance = stopDistance;
		if(type == EnemyType.Distance) { agent.autoBraking = false; } else { agent.autoBraking = true; }
		agent.avoidancePriority = Random.Range(30, 70);
		agent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance;
		player = FindObjectOfType<Player>();
	}
	void Start()
	{
		defaultScale = transform.localScale;
		if (enemyRenderer == null) 
		{ 
			enemyRenderer = GetComponent<Renderer>(); 
		}
		if (enemyRenderer != null)
		{
			skinnedRendererMaterial = enemyRenderer.materials;
			originalMaterials = enemyRenderer.materials;

		}
		target = GameObject.FindGameObjectWithTag("Player").transform;
		if (type == EnemyType.Explosive)
		{
			agent.stoppingDistance = explosiveStopDistance;
		}
		if (type == EnemyType.Distance)
		{
			agent.stoppingDistance = minimumSafeDistance;
		}
		else
		{
			agent.stoppingDistance = stopDistance;

		}
		StartCoroutine(SpawnerSolve());
	}

	void Update()
	{
		if (isDead) return;

		float distanceToTarget = Vector3.Distance(transform.position, target.position);
		switch (type)
		{
			case EnemyType.Basic:
				HandleBasicEnemyBehavior(distanceToTarget);
				break;
			case EnemyType.Explosive:
				HandleExplosiveEnemyBehavior(distanceToTarget);
				break;
			case EnemyType.Distance:
				HandleDistanceEnemyBehavior(distanceToTarget);
				break;
		}
	}

	void HandleBasicEnemyBehavior(float distance)
	{
		enemyShotSound = null;
		if (distance <= attackRange)
		{
			if (attackTimer <= 0)
			{
				Attack();
				attackTimer = attackInterval;
			}
			attackTimer -= Time.deltaTime;
		}
		else if (distance <= chaseRange)
		{
			ChaseTarget();
		}
		if(agent.velocity.magnitude < 0.1f)
		{
			animator.SetBool(Run, false);
		}
	}

	void HandleExplosiveEnemyBehavior(float distance)
	{
		enemyShotSound = null;
		transform.localScale = new Vector3(defaultScale.x * (scaleFactor * explosionTimer + 1f), defaultScale.y * (scaleFactor * explosionTimer + 1f), defaultScale.z * (scaleFactor * explosionTimer + 1f));
		if (distance <= explosionRange)
		{
			if (!isExploding)
			{
				isExploding = true;
				explosionTimer = 0f;
				agent.isStopped = true;
			}
			else if (explosionTimer < explosionDelay)
			{
				explosionTimer += Time.deltaTime;
			}
			else 
			{
				Explode();
				isExploding = false;
				agent.isStopped = false;
			}
		}
		else if (distance > explosionRange && isExploding) 
		{
			agent.isStopped = false;
			explosionTimer -= Time.deltaTime * 0.5f; 
			if (explosionTimer <= 0)
			{
				isExploding = false;  
			}
			ChaseTarget();
		}

		if (!isExploding && distance > explosionRange && distance <= chaseRange)
		{
			ChaseTarget();
		}
	}

	void HandleDistanceEnemyBehavior(float distance)
	{
		if (distance <= shootingRange && distance > minimumSafeDistance)
		{
			UpdateRotationTowardsTarget();
			animator.SetBool(Run, false);
			if (shootingTimer <= 0)
			{
				enemyShotSound.Play();

				animator.SetTrigger(AttackAnim);

				shootingTimer = shootingInterval;
			}
			shootingTimer -= Time.deltaTime;
		}
		else if (distance <= minimumSafeDistance)
		{
			FleeFromTarget();
		}
		else if (distance <= chaseRange)
		{
			ChaseTarget();
		}
		else
		{
			animator.SetBool(Run, false);
		}
	}

	void UpdateRotationTowardsTarget(bool away = false)
	{
		Vector3 direction = (away ? transform.position - target.position : target.position - transform.position).normalized;
		if (direction != Vector3.zero)
		{
			Quaternion lookRotation = Quaternion.LookRotation(direction);
			transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
		}
	}
	void FleeFromTarget()
	{
		Physics.IgnoreLayerCollision(gameObject.layer, LayerMask.NameToLayer("Enemy"), true);

		Vector3 fleeDirection = (transform.position - target.position).normalized;
		Vector3 fleeTarget = transform.position + fleeDirection * minimumSafeDistance;
		NavMeshHit hit;

		if (!NavMesh.SamplePosition(fleeTarget, out hit, minimumSafeDistance * 2, NavMesh.AllAreas))
		{
			bool foundEscapeRoute = false;
			float angleStep = 45;
			for (int i = 1; i <= 8; i++)
			{
				Vector3 newFleeDirection = Quaternion.Euler(0, angleStep * i, 0) * fleeDirection;
				Vector3 newFleeTarget = transform.position + newFleeDirection * minimumSafeDistance;
				if (NavMesh.SamplePosition(newFleeTarget, out hit, minimumSafeDistance * 2, NavMesh.AllAreas))
				{
					fleeTarget = hit.position;
					foundEscapeRoute = true;
					break;
				}
			}

			if (!foundEscapeRoute)
			{
				Debug.LogError("Failed to find escape route for enemy.");
				fleeTarget = transform.position - fleeDirection * minimumSafeDistance;
				NavMesh.SamplePosition(fleeTarget, out hit, minimumSafeDistance * 2, NavMesh.AllAreas);
			}
		}

		if (hit.position != Vector3.zero) 
		{
			agent.SetDestination(hit.position);
		}
		else
		{
			Debug.LogError("No valid flee position found; enemy might get stuck.");
		}

		animator.SetBool(Run, true);
		UpdateRotationTowardsTarget(true);
		Physics.IgnoreLayerCollision(gameObject.layer, LayerMask.NameToLayer("Enemy"), false);
	}

	public void ShootProjectile()
	{
		if (projectilePrefab && target)
		{
			Vector3 targetPosition = target.position + Vector3.up * 1.5f;
			Vector3 direction = (targetPosition - shootingOrigin.position).normalized;
			GameObject projectile = Instantiate(projectilePrefab, shootingOrigin.position, Quaternion.LookRotation(direction));
			projectile.GetComponent<EnemyProjectile>().Initialize(direction);
		}
	}

	void Attack()
	{
		animator.SetBool(Run, false);
		animator.SetTrigger(AttackAnim);
		// Animation event to toggle punch triggers
	}

	void ToggleLeftPunchTrigger()
	{
		LeftPunchTrigger punchTrigger = GetComponentInChildren<LeftPunchTrigger>();
		punchTrigger.enabled = !punchTrigger.enabled;
	}
	void ToggleRightPunchTrigger()
	{
		RightPunchTrigger punchTrigger = GetComponentInChildren<RightPunchTrigger>();
		punchTrigger.enabled = !punchTrigger.enabled;
	}

	void Explode()
	{
		
		//Debug.Log("Exploding!");
		//Apply damage to all players within explosionRange
		Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRange);
		sfxEnemy.Play();

		foreach (var collider in colliders)
		{
			if (collider.CompareTag("Player"))
			{
				collider.GetComponent<Player>().TakeDamage(explosionDamage);
			}
		}
		TakeDamage(10000f); //Die after an explosion with more damage than usual for a bigger Gore Simulator explosion force
	}

	void ChaseTarget()
	{
		agent.speed = moveSpeed;
		agent.SetDestination(target.position);
		if (type != EnemyType.Explosive) { animator.SetBool(Run, true); }
	}

	public void TakeDamage(float amount)
	{
		health -= amount;
		if (type != EnemyType.Explosive) { animator.SetTrigger(Hit); }
		StartCoroutine(FlashDamageEffect());
		StartCoroutine(HandleDamagePause());

		if (health <= 0f)
		{
			isDead = true;
			player.IncrementKillCount(type);
			enemyRenderer.materials = originalMaterials;
			Instantiate(bloodExplosion, bloodSpawnExplosion, transform);
			GoreEffect(amount);
			StartCoroutine(DissolveCo());
		}
	}
	void Die()
	{
		deathSound.Play();
		//GameManager.EnemyKilledEvent.Invoke();
		Destroy(gameObject);
	}

	void GoreEffect(float amount)
	{
		//Death gore effects
		var goreObject = GetComponent<GoreSimulator>();
		if (!goreObject) { Debug.LogError("No component found"); return; }
		goreObject.ExecuteExplosion(amount * 0.1f);
	}
	#region Coroutines

	IEnumerator FlashDamageEffect()
	{
		Material[] backupMaterials = new Material[enemyRenderer.materials.Length];
		for (int i = 0; i < enemyRenderer.materials.Length; i++)
		{
			backupMaterials[i] = enemyRenderer.materials[i];
		}

		Material[] flashMaterials = new Material[enemyRenderer.materials.Length];
		for (int i = 0; i < flashMaterials.Length; i++)
		{
			flashMaterials[i] = flashDamageMaterial;
		}
		enemyRenderer.materials = flashMaterials;
	
		yield return new WaitForSeconds(0.1f);  

		enemyRenderer.materials = backupMaterials;
	}
	IEnumerator HandleDamagePause()
	{
		agent.isStopped = true;
		attackTimer = attackInterval;
		yield return new WaitForSeconds(0.5f);
		if (health > 0) 
		{
			agent.isStopped = false;
		}
	}
	

	IEnumerator DissolveCo()
	{
		if (VFXGraph != null)
		{
			VFXGraph.Play();
		}
		if (type == EnemyType.Basic && animator != null)
		{
			animator.SetTrigger(isDeadAnim);
		}
		foreach (Collider c in GetComponentsInChildren<CapsuleCollider>())
		{
			c.enabled = false;
		}
		Instantiate(bloodExplosion, bloodSpawnExplosion.transform.position, Quaternion.identity);
		yield return new WaitForSeconds(dieDelay);
		while (skinnedRendererMaterial[0].GetFloat("_DissolveAmmount") < 1)
		{
			counter += dissolveRate;
			for (int i = 0; i < skinnedRendererMaterial.Length; i++)
			{
				skinnedRendererMaterial[i].SetFloat("_DissolveAmmount", counter);

			}
			yield return new WaitForSeconds(refreshRate);

		}
		if (skinnedRendererMaterial[0].GetFloat("_DissolveAmmount") >= 1)
		{
			Debug.Log(type + "Die");
			capsuleCollider.enabled = false;
			Die();
		}
	}
	IEnumerator SpawnerSolve()
	{
		if (VFXGraph != null)
		{
			VFXGraph.Play();
		}

		for (int i = 0; i < skinnedRendererMaterial.Length; i++)
		{
			skinnedRendererMaterial[i].SetFloat("_DissolveAmmount", 1);
		}

		yield return new WaitForSeconds(dieDelay);

		float ReverseCounter = 1;
		while (skinnedRendererMaterial[0].GetFloat("_DissolveAmmount") > 0)
		{
			ReverseCounter -= dissolveRate;
			for (int i = 0; i < skinnedRendererMaterial.Length; i++)
			{
				skinnedRendererMaterial[i].SetFloat("_DissolveAmmount", ReverseCounter);

			}
			yield return new WaitForSeconds(refreshRate);

		}

	}
	#endregion
	void OnDrawGizmosSelected()
	{
		//Drawing detection and attack ranges
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, attackRange);
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, chaseRange);

		//Drawing explosion range for explosive enemy
		if (type == EnemyType.Explosive)
		{
			Gizmos.color = Color.magenta;
			Gizmos.DrawWireSphere(transform.position, explosionRange);
		}
	}
}



