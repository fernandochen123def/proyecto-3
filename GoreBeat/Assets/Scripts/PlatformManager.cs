using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
	[Header("Platform Settings")]
	[SerializeField] private List<GameObject> platforms = new List<GameObject>();
	[SerializeField] private float moveDistance = 5.0f;
	[SerializeField] private float moveDuration = 1.0f;

	private bool isUp = false;
	private List<GameObject> activePlatforms = new List<GameObject>();

	public void MovePlatforms(int numberOfPlatforms)
	{
		if (isUp)
		{
			StartCoroutine(MovePlatformsCoroutine(activePlatforms, Vector3.down * moveDistance));
		}
		else
		{
			List<GameObject> newActivePlatforms = GetRandomPlatforms(numberOfPlatforms);
			StartCoroutine(MovePlatformsCoroutine(newActivePlatforms, Vector3.up * moveDistance));
			activePlatforms = newActivePlatforms;
		}

		isUp = !isUp;
	}

	private List<GameObject> GetRandomPlatforms(int count)
	{
		List<GameObject> randomPlatforms = new List<GameObject>(platforms);
		for (int i = randomPlatforms.Count - 1; i > count - 1; i--)
		{
			int randomIndex = Random.Range(0, i + 1);
			GameObject temp = randomPlatforms[randomIndex];
			randomPlatforms[randomIndex] = randomPlatforms[i];
			randomPlatforms[i] = temp;
		}
		randomPlatforms.RemoveRange(count, randomPlatforms.Count - count);
		return randomPlatforms;
	}

	private IEnumerator MovePlatformsCoroutine(List<GameObject> platformsToMove, Vector3 direction)
	{
		float elapsedTime = 0f;
		Vector3[] startPositions = new Vector3[platformsToMove.Count];
		for (int i = 0; i < platformsToMove.Count; i++)
		{
			startPositions[i] = platformsToMove[i].transform.position;
		}

		while (elapsedTime < moveDuration)
		{
			elapsedTime += Time.deltaTime;
			float t = elapsedTime / moveDuration;
			for (int i = 0; i < platformsToMove.Count; i++)
			{
				platformsToMove[i].transform.position = Vector3.Lerp(startPositions[i], startPositions[i] + direction, t);
			}
			yield return null;
		}

		for (int i = 0; i < platformsToMove.Count; i++)
		{
			platformsToMove[i].transform.position = startPositions[i] + direction;
		}
	}
}
