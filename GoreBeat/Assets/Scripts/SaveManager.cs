using System.IO;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
	public static SaveManager Singleton { get; private set; }
	string path;
	string resetPath;
	PlayerSave playerSave;

	private void Awake()
	{
		if (Singleton == null)
		{
			Singleton = this;
			// DontDestroyOnLoad(gameObject); // Already in sceneloadercomplex
			path = Application.streamingAssetsPath + "/Saves/Save.json";
			resetPath = Application.streamingAssetsPath + "/Saves/ResetSave.json";
			if (!File.Exists(path))
			{
				playerSave = new PlayerSave();
				Save();
			}
			else
			{
				Load();
			}
		}
		else
		{
			Debug.LogFormat("<color=orange>Duplicated component deleted</color>");
			Destroy(this);
		}
	}

	public void Save()
	{
		File.WriteAllText(path, JsonUtility.ToJson(playerSave));
	}

	public void Load()
	{
		playerSave = JsonUtility.FromJson<PlayerSave>(File.ReadAllText(path));
		// Example for direct use: player.transform.position = playerSave.position;
	}

	public void AllPerfectCheck()
	{
		Load();
		if(playerSave.gradeLevel1.Equals("S+") && playerSave.gradeLevel2.Equals("S+") && playerSave.gradeLevel3.Equals("S+"))
		{
			AchievementManager.Instance.UnlockAchievement("toogood");
		}
	}

	public void UnlockAll()
	{
		UnlockLevel(1);
		UnlockLevel(2);
		UnlockLevel(3);
		UnlockSecondFist();
		UnlockThirdFist();
		AchievementManager.Instance.UnlockOrResetAllAchievements(true);
	}

	public void ResetAll()
	{
		PlayerSave resetSave = JsonUtility.FromJson<PlayerSave>(File.ReadAllText(resetPath));
		File.WriteAllText(path, JsonUtility.ToJson(resetSave));
		AchievementManager.Instance.UnlockOrResetAllAchievements(false);
	}

	// Methods to update progression
	public void UnlockSecondFist()
	{
		playerSave.secondFistUnlocked = true;
		Save();
	}

	public void UnlockThirdFist()
	{
		playerSave.thirdFistUnlocked = true;
		Save();
	}

	public void UnlockLevel(int level)
	{
		switch (level)
		{
			case 1:
				AchievementManager.Instance.UnlockAchievement("tutorial");
				playerSave.firstLevelUnlocked = true;
				break;
			case 2:
				AchievementManager.Instance.UnlockAchievement("level1");
				playerSave.secondLevelUnlocked = true;
				break;
			case 3:
				AchievementManager.Instance.UnlockAchievement("level2");
				playerSave.thirdLevelUnlocked = true;
				break;
		}
		Save();
	}
	public void SaveAchievement(string achievementID, bool value)
	{
		switch (achievementID)
		{
			case "beatkakke":
				playerSave.beatkakke = value;
				break;
			case "completionist":
				playerSave.completionist = value;
				break;
			case "playerdeath":
				playerSave.playerdeath = value;
				break;
			case "exterminator":
				playerSave.exterminator = value;
				break;
			case "fell":
				playerSave.fell = value;
				break;
			case "firstenemy":
				playerSave.firstenemy = value;
				break;
			case "greatassets":
				playerSave.greatassets = value;
				break;
			case "heatmode":
				playerSave.heatmode = value;
				break;
			case "kachow":
				playerSave.kachow = value;
				break;
			case "level1":
				playerSave.level1 = value;
				break;
			case "level2":
				playerSave.level2 = value;
				break;
			case "level3":
				playerSave.level3 = value;
				break;
			case "perfect":
				playerSave.perfect = value;
				break;
			case "platform":
				playerSave.platform = value;
				break;
			case "speedrun":
				playerSave.speedrun = value;
				break;
			case "titan":
				playerSave.titan = value;
				break;
			case "toogood":
				playerSave.toogood = value;
				break;
			case "tutorial":
				playerSave.tutorial = value;
				break;
			case "untouchable":
				playerSave.untouchable = value;
				break;
			case "wastedpotential":
				playerSave.wastedpotential = value;
				break;
		}
		Save();
	}

	// Methods to update high scores
	public void UpdateHighScore(int level, int score, int streak, string timeSurvived, string grade)
	{
		Load();
		switch (level)
		{
			case 1:
				if (score > playerSave.scoreLevel1)
				{
					playerSave.scoreLevel1 = score;
					playerSave.highestStreakLevel1 = streak;
					playerSave.timeSurvivedLevel1 = timeSurvived;
					playerSave.gradeLevel1 = grade;
				}
				break;
			case 2:
				if (score > playerSave.scoreLevel2)
				{
					playerSave.scoreLevel2 = score;
					playerSave.highestStreakLevel2 = streak;
					playerSave.timeSurvivedLevel2 = timeSurvived;
					playerSave.gradeLevel2 = grade;
				}
				break;
			case 3:
				if (score > playerSave.scoreLevel3)
				{
					playerSave.scoreLevel3 = score;
					playerSave.highestStreakLevel3 = streak;
					playerSave.timeSurvivedLevel3 = timeSurvived;
					playerSave.gradeLevel3 = grade;
				}
				break;
		}
		Save();
	}

	// Getter methods to check progression variables
	public void LoadAchievements()
	{
		Load();
		AchievementManager.Instance.achievements.ForEach(achievement =>
		{
			switch (achievement.achievementID)
			{
				case "beatkakke":
					achievement.isUnlocked = playerSave.beatkakke;
					break;
				case "completionist":
					achievement.isUnlocked = playerSave.completionist;
					break;
				case "playerdeath":
					achievement.isUnlocked = playerSave.playerdeath;
					break;
				case "exterminator":
					achievement.isUnlocked = playerSave.exterminator;
					break;
				case "fell":
					achievement.isUnlocked = playerSave.fell;
					break;
				case "firstenemy":
					achievement.isUnlocked = playerSave.firstenemy;
					break;
				case "greatassets":
					achievement.isUnlocked = playerSave.greatassets;
					break;
				case "heatmode":
					achievement.isUnlocked = playerSave.heatmode;
					break;
				case "kachow":
					achievement.isUnlocked = playerSave.kachow;
					break;
				case "level1":
					achievement.isUnlocked = playerSave.level1;
					break;
				case "level2":
					achievement.isUnlocked = playerSave.level2;
					break;
				case "level3":
					achievement.isUnlocked = playerSave.level3;
					break;
				case "perfect":
					achievement.isUnlocked = playerSave.perfect;
					break;
				case "platform":
					achievement.isUnlocked = playerSave.platform;
					break;
				case "speedrun":
					achievement.isUnlocked = playerSave.speedrun;
					break;
				case "titan":
					achievement.isUnlocked = playerSave.titan;
					break;
				case "toogood":
					achievement.isUnlocked = playerSave.toogood;
					break;
				case "tutorial":
					achievement.isUnlocked = playerSave.tutorial;
					break;
				case "untouchable":
					achievement.isUnlocked = playerSave.untouchable;
					break;
				case "wastedpotential":
					achievement.isUnlocked = playerSave.wastedpotential;
					break;
			}
		});
	}

	public bool IsSecondFistUnlocked()
	{
		Load();
		return playerSave.secondFistUnlocked;
	}

	public bool IsThirdFistUnlocked()
	{
		Load();
		return playerSave.thirdFistUnlocked;
	}

	public bool IsFirstLevelUnlocked()
	{
		Load();
		return playerSave.firstLevelUnlocked;
	}

	public bool IsSecondLevelUnlocked()
	{
		Load();
		return playerSave.secondLevelUnlocked;
	}

	public bool IsThirdLevelUnlocked()
	{
		Load();
		return playerSave.thirdLevelUnlocked;
	}
	
	public int GetHighScore(int level)
	{
		Load();
		switch (level)
		{
			case 1:
				return playerSave.scoreLevel1;
			case 2:
				return playerSave.scoreLevel2;
			case 3:
				return playerSave.scoreLevel3;
			default:
				return 0;
		}
	}

	public string GetTimeSurvived(int level)
	{
		Load();
		switch (level)
		{
			case 1:
				return playerSave.timeSurvivedLevel1;
			case 2:
				return playerSave.timeSurvivedLevel2;
			case 3:
				return playerSave.timeSurvivedLevel3;
			default:
				return "00:00";
		}
	}

	public int GetHighestStreak(int level)
	{
		Load();
		switch (level)
		{
			case 1:
				return playerSave.highestStreakLevel1;
			case 2:
				return playerSave.highestStreakLevel2;
			case 3:
				return playerSave.highestStreakLevel3;
			default:
				return 0;
		}
	}

	public string GetGrade(int level)
	{
		Load();
		switch (level)
		{
			case 1:
				return playerSave.gradeLevel1;
			case 2:
				return playerSave.gradeLevel2;
			case 3:
				return playerSave.gradeLevel3;
			default:
				return "N/A";
		}
	}
}

[System.Serializable]
public class PlayerSave
{
	// --- PROGRESSION ---
	// Fists
	public bool secondFistUnlocked;
	public bool thirdFistUnlocked;

	// Levels
	public bool firstLevelUnlocked; // Tutorial completed
	public bool secondLevelUnlocked;
	public bool thirdLevelUnlocked;

	// --- HIGHSCORES ---
	// Level 1
	public int scoreLevel1;
	public int highestStreakLevel1;
	public string timeSurvivedLevel1;
	public string gradeLevel1;

	// Level 2
	public int scoreLevel2;
	public int highestStreakLevel2;
	public string timeSurvivedLevel2;
	public string gradeLevel2;

	// Level 3
	public int scoreLevel3;
	public int highestStreakLevel3;
	public string timeSurvivedLevel3;
	public string gradeLevel3;

	// --- ACHIEVEMENTS ---
	public bool beatkakke;
	public bool completionist;
	public bool playerdeath;
	public bool exterminator;
	public bool fell;
	public bool firstenemy;
	public bool greatassets;
	public bool heatmode;
	public bool kachow;
	public bool level1;
	public bool level2;
	public bool level3;
	public bool perfect;
	public bool platform;
	public bool speedrun;
	public bool titan;
	public bool toogood;
	public bool tutorial;
	public bool untouchable;
	public bool wastedpotential;
}
