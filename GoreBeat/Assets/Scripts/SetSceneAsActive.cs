using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetSceneAsActive : MonoBehaviour
{
    void Start()
    {
		SceneManager.SetActiveScene(this.gameObject.scene);
	}
}
