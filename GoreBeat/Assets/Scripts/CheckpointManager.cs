using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
	public static CheckpointManager Instance { get; private set; }

	private Transform currentCheckpoint;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			//DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	public void SetCheckpoint(Transform checkpoint)
	{
		currentCheckpoint = checkpoint;
	}

	public Transform GetCheckpoint()
	{
		return currentCheckpoint;
	}
}
