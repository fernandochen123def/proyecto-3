using System.Collections;
using UnityEngine;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

namespace StarterAssets
{
	[RequireComponent(typeof(CharacterController))]
#if ENABLE_INPUT_SYSTEM
	[RequireComponent(typeof(PlayerInput))]
#endif
	public class FirstPersonController : MonoBehaviour
	{
		[Header("Player")]
		[Tooltip("Move speed of the character in m/s")]
		public float MoveSpeed = 4.0f;
		[Tooltip("Sprint speed of the character in m/s")]
		public float SprintSpeed = 6.0f;
		[Tooltip("Rotation speed of the character")]
		public float RotationSpeed = 1.0f;
		[Tooltip("Acceleration and deceleration")]
		public float SpeedChangeRate = 10.0f;

		[Space(10)]
		[Tooltip("The height the player can jump")]
		public float JumpHeight = 1.2f;
		[Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
		public float Gravity = -15.0f;

		[Space(10)]
		[Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
		public float JumpTimeout = 0.1f;
		[Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
		public float FallTimeout = 0.15f;

		[Header("Player Grounded")]
		[Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
		public bool Grounded = true;
		[Tooltip("Useful for rough ground")]
		public float GroundedOffset = -0.14f;
		[Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
		public float GroundedRadius = 0.5f;
		[Tooltip("What layers the character uses as ground")]
		public LayerMask GroundLayers;

		[Header("Cinemachine")]
		[Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
		public GameObject CinemachineCameraTarget;
		[Tooltip("How far in degrees can you move the camera up")]
		public float TopClamp = 90.0f;
		[Tooltip("How far in degrees can you move the camera down")]
		public float BottomClamp = -90.0f;

		// cinemachine
		private float _cinemachineTargetPitch;

		// player
		private float _speed;
		private float _rotationVelocity;
		private float _verticalVelocity;
		//private float _terminalVelocity = 53.0f;

		// timeout deltatime
		private float _jumpTimeoutDelta;
		private float _fallTimeoutDelta;

		private PauseMenu pauseMenu;


#if ENABLE_INPUT_SYSTEM
		private PlayerInput _playerInput;
#endif
		private CharacterController _controller;
		public StarterAssetsInputs input;
		//private GameObject _mainCamera;

		private const float _threshold = 0.0001f;

		private bool IsCurrentDeviceMouse
		{
			get
			{
				#if ENABLE_INPUT_SYSTEM
				return _playerInput.currentControlScheme == "KeyboardMouse";
				#else
				return false;
				#endif
			}
		}

		private void Awake()
		{
			pauseMenu = FindAnyObjectByType<PauseMenu>();

		}

		private void Start()
		{
			_controller = GetComponent<CharacterController>();
			input = GetComponent<StarterAssetsInputs>();
#if ENABLE_INPUT_SYSTEM
			_playerInput = GetComponent<PlayerInput>();
#else
			Debug.LogError( "Starter Assets package is missing dependencies. Please use Tools/Starter Assets/Reinstall Dependencies to fix it");
#endif

			// reset our timeouts on start
			_jumpTimeoutDelta = JumpTimeout;
			_fallTimeoutDelta = FallTimeout;
		}

		private void Update()
		{
			JumpAndGravity();
			GroundedCheck();
			Move();
		}

		private void LateUpdate()
		{
			CameraRotation();
		}

		public void GroundedCheck()
		{
			// set sphere position, with offset
			Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
			Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);
		}

		public Vector3 testingValue;
		public Vector3 defaultValue;
		private void CameraRotation()
		{
			// if there is an input
			if (input.look.sqrMagnitude >= _threshold && !FindObjectOfType<PauseMenu>().isPaused) 
			{
				//Don't multiply mouse input by Time.deltaTime
				float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;
				
				_cinemachineTargetPitch += input.look.y * RotationSpeed * deltaTimeMultiplier;
				_rotationVelocity = input.look.x * RotationSpeed * deltaTimeMultiplier;

				// clamp our pitch rotation
				_cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

				Vector3 testVector = Vector3.zero;
				if (Input.GetKey(KeyCode.Y))
				{
					testVector = testingValue;
				}
				// Update Cinemachine camera target pitch
				CinemachineCameraTarget.transform.localRotation = Quaternion.Euler(testingValue.x + _cinemachineTargetPitch, testingValue.y, testingValue.z);

				// rotate the player left and right
				transform.Rotate(Vector3.up * _rotationVelocity);
			}
		}

		private Coroutine lookAtGroundCoroutine;
		private Coroutine resetCameraCoroutine;

		public void LookDownAtGround()
		{
			if (lookAtGroundCoroutine != null)
			{
				StopCoroutine(lookAtGroundCoroutine);
			}
			if (resetCameraCoroutine != null)
			{
				StopCoroutine(resetCameraCoroutine);
			}
			lookAtGroundCoroutine = StartCoroutine(SmoothLookAtGround());
		}

		public void ResetCameraPosition()
		{
			if (lookAtGroundCoroutine != null)
			{
				StopCoroutine(lookAtGroundCoroutine);
			}
			if (resetCameraCoroutine != null)
			{
				StopCoroutine(resetCameraCoroutine);
			}
			resetCameraCoroutine = StartCoroutine(SmoothResetCameraPosition());
		}

		private IEnumerator SmoothLookAtGround()
		{
			float targetPitch = 60.0f;
			float duration = 0.5f; // Duration of the transition
			float elapsedTime = 0f;
			float startPitch = _cinemachineTargetPitch;

			while (elapsedTime < duration)
			{
				elapsedTime += Time.deltaTime;
				_cinemachineTargetPitch = Mathf.Lerp(startPitch, targetPitch, elapsedTime / duration);
				CinemachineCameraTarget.transform.localRotation = Quaternion.Euler(_cinemachineTargetPitch, 0.0f, 0.0f);
				yield return null;
			}

			_cinemachineTargetPitch = targetPitch;
			CinemachineCameraTarget.transform.localRotation = Quaternion.Euler(_cinemachineTargetPitch, 0.0f, 0.0f);
		}

		private IEnumerator SmoothResetCameraPosition()
		{
			float targetPitch = 0.0f;
			float duration = 0.25f; // Duration of the transition
			float elapsedTime = 0f;
			float startPitch = _cinemachineTargetPitch;

			while (elapsedTime < duration)
			{
				elapsedTime += Time.deltaTime;
				_cinemachineTargetPitch = Mathf.Lerp(startPitch, targetPitch, elapsedTime / duration);
				CinemachineCameraTarget.transform.localRotation = Quaternion.Euler(_cinemachineTargetPitch, 0.0f, 0.0f);
				yield return null;
			}

			_cinemachineTargetPitch = targetPitch;
			CinemachineCameraTarget.transform.localRotation = Quaternion.Euler(_cinemachineTargetPitch, 0.0f, 0.0f);
		}

		private Vector3 _velocity;  // Declare this at the class level

		private void Move()
		{
			// Get the input direction relative to the camera's orientation
			Vector3 inputDirection = Vector3.zero;
			if (input.move != Vector2.zero)
			{
				// Transform the input direction from local space to world space relative to the camera's orientation
				inputDirection = PlayerCameraManager.PlayerCamera.transform.right * input.move.x + PlayerCameraManager.PlayerCamera.transform.forward * input.move.y;
				inputDirection.y = 0.0f; // Ensure movement is strictly horizontal
				inputDirection.Normalize(); // Normalize to maintain consistent movement speed in all directions
			}

			// Determine the target speed based on whether the player is sprinting
			float targetSpeed = input.sprint ? SprintSpeed : MoveSpeed;

			// Apply the target speed to the input direction
			Vector3 targetVelocity = inputDirection * targetSpeed;

			// Interpolate the current velocity towards the target velocity
			if (Grounded)
			{
				// Use stronger interpolation if grounded, ensuring responsive control
				_velocity.x = Mathf.Lerp(_velocity.x, targetVelocity.x, Time.deltaTime * SpeedChangeRate);
				_velocity.z = Mathf.Lerp(_velocity.z, targetVelocity.z, Time.deltaTime * SpeedChangeRate);
			}
			else
			{
				// Apply weaker interpolation in air to simulate air control
				_velocity.x = Mathf.Lerp(_velocity.x, targetVelocity.x, Time.deltaTime * SpeedChangeRate * 0.2f);
				_velocity.z = Mathf.Lerp(_velocity.z, targetVelocity.z, Time.deltaTime * SpeedChangeRate * 0.2f);
			}

			// Always apply gravity to ensure the character falls
			_velocity.y += Gravity * Time.deltaTime;

			// Move the character controller
			_controller.Move(_velocity * Time.deltaTime);
		}


		public void JumpAndGravity(bool jump = false)
		{
			if (Grounded && jump && _jumpTimeoutDelta <= 0.0f)
			{
				_velocity.y = Mathf.Sqrt(JumpHeight * -2f * Gravity);
			}

			_jumpTimeoutDelta -= Time.deltaTime;
			if (!Grounded)
			{
				_fallTimeoutDelta -= Time.deltaTime;
			}

			if (_fallTimeoutDelta <= 0.0f && !_controller.isGrounded)
			{
				Grounded = false;
			}
		}


		private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
		{
			if (lfAngle < -360f) lfAngle += 360f;
			if (lfAngle > 360f) lfAngle -= 360f;
			return Mathf.Clamp(lfAngle, lfMin, lfMax);
		}

		private void OnDrawGizmosSelected()
		{
			Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
			Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

			if (Grounded) Gizmos.color = transparentGreen;
			else Gizmos.color = transparentRed;

			// when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
			Gizmos.DrawSphere(new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z), GroundedRadius);
		}
	}
}